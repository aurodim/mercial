const Liana = require('forest-express-mongoose');

Liana.collection('mercials', {
  actions: [{
    name: 'Set Moderated',
    endpoint: '/api/admin/actions/moderate',
    fields: [
      {
        field: 'reject',
        default: false,
        type: 'Boolean',
        description: 'Reject this Mercial?',
      }
    ]
  }],
});

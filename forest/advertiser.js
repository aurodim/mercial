const Liana = require('forest-express-mongoose');

Liana.collection('creators', {
  actions: [{
    name: 'Add Credits',
    endpoint: '/api/admin/actions/acc',
    fields: [
      {
        field: 'amount',
        type: 'Number',
        description: 'Amount of Credits to Add to this Account',
        isRequired: true
      }
    ]
  }],
});

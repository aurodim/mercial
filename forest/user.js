const Liana = require('forest-express-mongoose');

Liana.collection('users', {
  actions: [{
    name: 'Toggle Merchips',
    endpoint: '/api/admin/actions/ame',
    fields: [
      {
        field: 'amount',
        type: 'Number',
        description: 'Amount of Merchips to Add to this Account',
        isRequired: true
      },
      {
        field: 'remove',
        type: 'Boolean',
        description: "Remove the amount?",
        isRequired: false
      }
    ]
  }],
});

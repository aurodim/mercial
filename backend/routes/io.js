const User = require('../models/user');
const Franchises = require('../controllers/franchises');
const Offers = require('../controllers/offers');
var jwt = require('jsonwebtoken');
const moment = require('moment');
exports = {};
exports.respond = Respond;
exports.connectionsManager = ConnectionsManager;
module.exports = exports;

/*

  EMITTING TO A FRANCHISE

  {
    _s : success ?
    _m : message
    _d : date
  }

*/

function ConnectionsManager() {
  global.io.sockets.on('connection', (socket) => {
    socket.on('validateAttemptToRedeem', (codeData) => {

      jwt.verify(codeData._a, process.env.JWT_F_SECRET, function(error, decoded) {
        if (error) return;

        if (typeof codeData._o === "undefined" || typeof codeData._f === "undefined" || typeof codeData._d === "undefined" || typeof codeData._channel === "undefined" || typeof codeData._c === "undefined") return;

        Franchises.one({_id : codeData._f, crypto: decoded._tk}).then((franchise) => {
          if (!franchise) throw new Error("No Franchise");

          return Offers.one({_id : codeData._o, franchise : codeData._f});
        }).then((offer) => {
          if (!offer) throw new Error("No Offer");

          return User.findOne({redeem_validator : codeData._c}).exec();
        }).then((user) => {
          if (!user) throw new Error("No User");

          global.io.emit(user.socket, codeData);
        }).catch((error) => {
          socket.emit(codeData._channel, {
            _s : false,
            _m : "_verificationerror",
            _d : moment().valueOf()
          });
        });
      });
    });
    socket.on('validateAttemptToRedeemByManager', (codeData) => {

      if (codeData._a.indexOf("__manager") == -1 || !codeData._m || !codeData._m._c) throw new Error("_manager_invalid");

      if (typeof codeData._o === "undefined" || typeof codeData._f === "undefined" || typeof codeData._d === "undefined" || typeof codeData._channel === "undefined" || typeof codeData._c === "undefined") throw new Error("_invalid_code_data");

      Franchises.one({_id : codeData._f, "manage.code" : codeData._m._c}).then((franchise) => {
        if (!franchise) throw new Error("_re_franchise_mismatch");

        return Offers.one({_id : codeData._o, franchise : codeData._f});
      }).then((offer) => {
        if (!offer) throw new Error("_re_offer_invalid");

        return User.findOne({redeem_validator : codeData._c}).exec();
      }).then((user) => {
        if (!user) throw new Error("_re_user_invalid");

        global.io.emit(user.socket, codeData);
      }).catch((error) => {
        socket.emit(codeData._channel, {
          _s : false,
          _m : "_verificationerror",
          _e : error,
          _d : moment().valueOf()
        });
      });
    });
  });
}

function Respond(c, s, m, e = null, r = null) {
  global.io.emit(c, {
    _s : s,
    _m : m,
    _r : r,
    _d : moment().valueOf(),
    _e : e
  });
}

const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const multer = require('multer');
const Admin = require('../controllers/admin.js');
const Users = require('../controllers/users.js');
const Franchises = require('../controllers/franchises.js');
const Advertisers = require('../controllers/advertisers.js');
const path = require("path");
const liana = require('forest-express-mongoose');
const DIR = path.join(process.cwd(), '/tmp');
const requestIp = require('request-ip');
const fs = require('fs');
const moment = require('moment');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, DIR);
  },
  filename: function (req, file, cb) {
    if (!fs.existsSync(DIR)){
        fs.mkdirSync(DIR);
    }
    cb(null, file.fieldname + '-' + moment().valueOf() + '.' + file.originalname.split(".")[1]);
  }
});

router.use(function (err, req, res, next) {
  if (err.code !== 'EBADCSRFTOKEN') return next(err);

  HandleError(res, 403);
});

// <editor-fold> ADMIN

router.post('/admin/stats/mbr', liana.ensureAuthenticated, Admin.merchipsBasedRevenue);

router.post('/admin/stats/par', liana.ensureAuthenticated, Admin.projectedAnnualRevenue);

router.post('/admin/stats/usv', liana.ensureAuthenticated, Admin.userSecondsViewed);

router.post('/admin/stats/av', liana.ensureAuthenticated, Admin.advertisersViews);

router.post('/admin/actions/acc', liana.ensureAuthenticated, Admin.addCredits);

router.post('/admin/actions/ame', liana.ensureAuthenticated, Admin.toggleMerchips);

router.post('/admin/actions/moderated', liana.ensureAuthenticated, Admin.moderateMercial);

router.post('/admin/actions/login/initiate', function(req, res) {
  if (!(req.body.key)) {
    return HandleError(res, 401);
  }

  Admin.loginAttempt(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post('/admin/actions/login', function(req, res) {
  if (!(req.body.key) || !(req.body.password)) {
    return HandleError(res, 401);
  }

  Admin.login(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.get('/admin/stats/home', function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_M_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Admin.dashboardData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get('/admin/stats/offers', function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_M_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Admin.offersData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get('/admin/stats/videos', function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_M_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Admin.mercialsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get('/admin/stats/reports', function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_M_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Admin.reportsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/admin/actions/offer/reject", function(req, res) {
  if (!(req.body.id) || !(req.body.reason) || !(req.body.expand) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Admin.rejectOffer(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/admin/actions/offer/approve", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Admin.approveOffer(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/admin/actions/video/reject", function(req, res) {
  if (!(req.body.id) || !(req.body.reason) || !(req.body.expand) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Admin.rejectVideo(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/admin/actions/video/approve", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Admin.approveVideo(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post('/clover/auth/hook', function(req, res) {
  HandleSuccess(res, {_m : "Webhook Success"});
});

// </editor-fold>

// <editor-fold> CLIENT
router.post("/user/register", function(req, res) {
  if (!(req.body.fullname) || !(req.body.email) || !(req.body.password) || !(req.body.birthdate)) {
    return HandleError(res, 401);
  }

  Users.createUser(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/login", function(req, res) {
  if (!(req.body.email) || !(req.body.password)) {
    return HandleError(res, 401);
  }

  Users.loginUser(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/forgot", function(req, res) {
  if (!(req.body.email)  || !(req.body.birthdate)) {
    return HandleError(res, 401);
  }

  Users.forgot(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/reset", function(req, res) {
  if (!(req.body.code) || !(req.body.email) || !(req.body["new-password"])) {
    return HandleError(res, 401);
  }

  Users.reset(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// <editor-fold> Analytics
router.get("/user/analytics", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Users.analyticsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>
// <editor-fold PURCHASES
router.post("/user/purchases/new", function(req, res) {
  if (!(req.body.data) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.purchase(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>
// <editor-fold> REPORT AND rate
router.post("/user/report", function(req, res) {
  if (!(req.body.id) || !(req.body.type) || !(req.body.reason) || typeof req.body.expand == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  if (req.body.type !== "mercial" && req.body.type !== "offer") {
    return HandleError(res, 401);
  }

  Users.report(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/rate", function(req, res) {
  if (!(req.body.id) || !(req.body.rating) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.rate(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/zes", function(req, res) {
  if (!(req.body.id) || !(req.body.location) || !(req.body.rid) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.zes(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

// </editor-fold>
// <editor-fold> FEED
router.get("/user/feed", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Users.feedData(req.query, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/user/feed/:id", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Users.feedItem(req.query, req.params.id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/user/feed/:id/interest", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  if (req.body.id != req.params.id) {
    return HandleError(res, 401);
  }

  Users.addInterest(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/feed/:id/watched", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  if (req.body.id != req.params.id) {
    return HandleError(res, 401);
  }

  Users.watchedMercial(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> HELP
router.get("/faqs", function(req, res) {
  const types = req.query.types;

  Users.helpData(types, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> REVIEWS
router.get("/user/reviews", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Users.reviewsData(req.query, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/user/reviews/:id/answered", function(req, res) {
  if (!(req.body.id) || typeof req.body.rating === "undefined" || typeof req.body.experience == "undefined" || typeof req.body.recommend == "undefined" || typeof req.body.recExplain == "undefined" || req.body.addition == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.reviewAnswered(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

// <editor-fold> OFFERS
router.get("/user/offers", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Users.offersData(req.query, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/user/offers/:id/fqr", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Users.fastQR(auth, req.params.id, function(error, response) {
      if (error) return HandleError(res, error.code);
      res.status(200).sendFile(response, (error) => {
        fs.unlink(response, () => {});
      });
    });
  });
});

router.get("/user/offers/:id", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Users.offersItem(auth, req.params.id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/user/offers/validate", function(req, res) {
  if (!(req.body.codeData) || typeof req.body.codeData._a === "undefined" || typeof req.body.codeData._c === "undefined" || typeof req.body.codeData._channel === "undefined" ||
  typeof req.body.codeData._d === "undefined" || typeof req.body.codeData._f === "undefined" || typeof req.body.codeData._o === "undefined" ||
  typeof req.body.codeData._u === "undefined" || typeof req.body.codeData._a === "undefined" || !(req.body.auth)) {

    return HandleError(res, 401);
  }

  if (req.body.auth !== req.body.codeData._u._a) {
    return HandleError(res, 500);
  }

  Users.validateRedeem(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> SETTINGS
router.get("/user/settings", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Users.settingsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/user/settings/account", function(req, res) {
  if (!(req.body.email) || typeof req.body.password == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.updateAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/settings/referral", function(req, res) {
  if (!(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.createReferral(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/settings/target", function(req, res) {
  if (!(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.updateTarget(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/settings/location", function(req, res) {
  if (typeof req.body.zipcode == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.updateLocation(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/settings/prefs", function(req, res) {
  if (typeof req.body.tags == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.updatePreferences(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/user/settings/privacy", function(req, res) {
  if (typeof req.body.aezp == "undefined" || typeof req.body.notif == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.updatePrivacy(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>
router.post("/user/delete", function(req, res) {
  if (!(req.body.email) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Users.deleteAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> FRANCHISE
router.post("/franchise/register", requestIp.mw(), function(req, res) {
  if (!(req.body.fullname) || !(req.body.email) || !(req.body.password) || !(req.body["franchise-name"]) || !(req.body["franchise-address"]) || !(req.body["franchise-country"]) || !(req.body["franchise-lat"]) || !(req.body["franchise-lng"]) || !(req.body["franchise-postal"])) {
    return HandleError(res, 401);
  }

  req.body.ip = req.clientIp;

  Franchises.createFranchise(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/login", function(req, res) {
  if (!(req.body.email) || !(req.body.password)) {
    return HandleError(res, 401);
  }

  Franchises.loginFranchise(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/forgot", function(req, res) {
  if (!(req.body.email) || !(req.body.franchise)) {
    return HandleError(res, 401);
  }

  Franchises.forgot(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/reset", function(req, res) {
  if (!(req.body.code) || !(req.body.email) || !(req.body["new-password"])) {
    return HandleError(res, 401);
  }

  Franchises.reset(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// <editor-fold> ANALYTICS
router.get("/franchise/analytics", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.analyticsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>

// <editor-fold> HELP
router.get("/franchise/help", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.helpData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>

// <editor-fold> Locations
router.get("/franchise/locations", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Franchises.locationsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>

// <editor-fold> Offers
router.get("/franchise/offers", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.offersData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/manager/offers", function(req, res) {
  if(!req.query.identifier || !req.query.manager) {
    return HandleError(res, 401);
  }

  Franchises.offersDataManager(req.query, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.get("/franchise/offers/analytics", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.offer;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.offerAnalyticsData(auth, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/franchise/offers/edit", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.offer;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.editorData(auth, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/franchise/offers/new", multer({storage: storage}).single('thumbnail'), function(req, res) {
  if (!(req.body.title) || typeof req.body.description == "undefined" || typeof req.file == "undefined" || !(req.body.savings) || typeof req.body.upc == "undefined" || !(req.body.refund) || typeof req.body.locations == "undefined") {
    return HandleError(res, 401);
  }

  Franchises.uploadOffer(req.body, req.file, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/offers/pause", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.pauseOffer(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/offers/edit", function(req, res) {
  if (!(req.body.id) || !(req.body.title) || typeof req.body.description == "undefined" || !(req.body.savings) || !(req.body.refund) || !(req.body.upc) || typeof req.body.locations == "undefined") {
    return HandleError(res, 401);
  }

  Franchises.updateOffer(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/offers/delete", function(req, res) {
  if (!(req.body.id) || !(req.body.d) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.deleteOffer(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> REVIEWS
router.get("/franchise/reviews", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.reviewsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/franchise/reviews/analytics", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.offer;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.reviewAnalyticsData(auth, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/franchise/reviews/edit", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.review;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.editorReviewData(auth, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/franchise/reviews/new", multer({storage: storage}).single('thumbnail'), function(req, res) {
  if (!(req.body.title) || typeof req.body.description == "undefined" || !(req.body.questions) || req.body.questions == "" || typeof req.file == "undefined") {
    return HandleError(res, 401);
  }

  Franchises.uploadReview(req.body, req.file, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/review/edit", function(req, res) {
  if (!(req.body.id) || !(req.body.title) || typeof req.body.description == "undefined") {
    return HandleError(res, 401);
  }

  Franchises.updateReview(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/review/delete", function(req, res) {
  if (!(req.body.id) || !(req.body.d) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.deleteReview(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> REEDEM
router.get("/franchise/redeems", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Franchises.redeemsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>

// <editor-fold> SETTINGS
router.get("/franchise/settings", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_F_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Franchises.settingsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/franchise/settings/account", function(req, res) {
  if (!(req.body.email) || typeof req.body.password == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.updateAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/franchise", function(req, res) {
  if (!(req.body.name) || !(req.body.owner) || typeof req.body.website == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.updateFranchise(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/square/authenticate", function(req, res) {
  if (!req.body.accessCode || !req.body.code) {
    return HandleError(res, 401);
  }

  Franchises.verifySquare(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/clover/authenticate", function(req, res) {
  if (!req.body.accessCode || !req.body.merchantID || !req.body.employeeID || !req.body.clientID || !req.body.code) {
    return HandleError(res, 401);
  }

  Franchises.verifyClover(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/stripe/verify", function(req, res) {
  if (!req.body.state || !req.body.code) {
    return HandleError(res, 401);
  }

  Franchises.verifyStripe(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/location", function(req, res) {
  if (typeof req.body.address == "undefined" || typeof req.body.lat == "undefined" || typeof req.body.lng == "undefined" || typeof req.body.postal == "undefined" || typeof req.body.previous == "undefined" || !(req.body.auth)) {
    return HandleError(res, 301);
  }

  Franchises.updateLocation(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/managers", function(req, res) {
  if (!(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.updateManagers(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/pos", function(req, res) {
  if (!(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.updatePos(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/redeem", function(req, res) {
  if (!(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.redeem(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/franchise/settings/privacy", function(req, res) {
  if (typeof req.body.notif == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.updatePrivacy(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>
router.post("/franchise/delete", function(req, res) {
  if (!(req.body.email) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Franchises.deleteAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> ADVERTISER
router.post("/advertiser/register", function(req, res) {
  if (!(req.body.fullname) || !(req.body.email)) {
    return HandleError(res, 401);
  }

  Advertisers.createAdvertiser(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/advertiser/login", function(req, res) {
  if (!(req.body.email) || !(req.body.accesskey)) {
    return HandleError(res, 401);
  }
  Advertisers.loginAdvertiser(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);
    HandleSuccess(res, response);
  });
});
// <editor-fold> Analytics
router.get("/advertiser/analytics", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Advertisers.analyticsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>
// <editor-fold> HELP
router.get("/advertiser/help", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Advertisers.helpData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});
// </editor-fold>
// <editor-fold> MERCIALS
router.get("/advertiser/mercials", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Advertisers.mercialsData(req.query, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/advertiser/mercials/new", multer({storage: storage}).single('video'), function(req, res) {
  if (!(req.body.title) || typeof req.body.description == "undefined" || typeof req.body.more == "undefined" || typeof req.body.moreSecond == "undefined" || typeof req.body.moreThird == "undefined" || typeof req.body.moreFourth == "undefined"
   || typeof req.body.moreFifth == "undefined" || typeof req.body.moreSixth == "undefined" || typeof req.body.tags == "undefined" || typeof req.body.size == "undefined" || typeof req.body.age == "undefined" || typeof req.file == "undefined"
    || typeof req.body.ethnicities == "undefined" || typeof req.body.locationType == "undefined" || typeof req.body.locations == "undefined" || !(req.body.auth)) {
    return HandleError(res, 401);
  }
  Advertisers.uploadMercial(req.body, req.file, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.get("/advertiser/mercials/edit", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.mercial;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Advertisers.editorData(req.query, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.get("/advertiser/mercials/analytics", function(req, res) {
  const auth = req.query.auth;
  const id = req.query.mercial;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);

    Advertisers.mercialAnalyticsData(auth, id, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/advertiser/mercials/edit", function(req, res) {
  if (!(req.body.title) || typeof req.body.description == "undefined" || typeof req.body.more == "undefined" || typeof req.body.tags == "undefined" || typeof req.body.size == "undefined" || !(req.body.auth)) {
    return HandleError(res, 301);
  }

  Advertisers.updateMercial(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/advertiser/mercials/pause", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertisers.pauseMercial(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/advertiser/mercials/delete", function(req, res) {
  if (!(req.body.id) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertisers.deleteMercial(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>
// <editor-fold> SETTINGS
router.get("/advertiser/settings", function(req, res) {
  const auth = req.query.auth;

  jwt.verify(auth, process.env.JWT_A_SECRET, function(error, decoded) {
    if (error) return HandleError(res, 401);


    Advertisers.settingsData(auth, function(error, response) {
      if (error) return HandleError(res, error.code);

      HandleSuccess(res, response);
    });
  });
});

router.post("/advertiser/settings/account", function(req, res) {
  if (!(req.body.email) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertisers.updateAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/advertiser/settings/info", function(req, res) {
  if (!(req.body.name) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertisers.updateInfo(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});

router.post("/advertiser/settings/payment", function(req, res) {
  if (typeof req.body["cc-name"] == "undefined" || !(req.body.token) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertisers.updatePayment(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>
router.post("/advertiser/delete", function(req, res) {
  if (!(req.body.email) || !(req.body.auth)) {
    return HandleError(res, 401);
  }

  Advertiser.deleteAccount(req.body, function(error, response) {
    if (error) return HandleError(res, error.code);

    HandleSuccess(res, response);
  });
});
// </editor-fold>

// <editor-fold> HANDLERS
function HandleError(res, code, error = null) {
  let httpCode = code;
  if (code === 401) {
    message = "Parameters are missing or unsupported";
  } else if (code === 402) {
    message = "Unathorized use of API";
  } else if (code === 403) {
    message = "Forbidden access";
  } else if (code === 500) {
    message = "Server error while processing request";
  } else {
    message = code;
    httpCode = 500;
  }
  let errorResponse = {
      "_hec" : code,
      "_rm" : message,
      "_e" : error | "error",
      "_d" : moment().valueOf()
  };
  res.status(httpCode).json(errorResponse);
}

function HandleSuccess(res, response) {
  let successResponse = {
    "_hsc" : 200,
    "_body" : response,
    "_d" : moment().valueOf()
  };
  res.status(200).json(successResponse);
}

// </editor-fold>

module.exports = router;

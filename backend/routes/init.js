const express = require('express');
const router = express.Router();
const apiRoutes = require('./api.js');
const path = require('path');

// API ROUTES
router.use("/api", apiRoutes);
/* GET home page. */
router.get('*', function(req, res, next) {
  res.sendFile(path.join(__dirname + '/../../dist/mercial/index.html'));
});

module.exports = router;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./user');
var Franchise = require('./franchise');

var redeemedSchema = new Schema({
  franchise : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Business" },
  offer : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Offer" },
  redeemer : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "User" },
  zipcode : { type : String, unique : false, required : false},
  saved : { type : Number, unique : false, required : true, default : 0, min : 0},
  revenue : { type : Number, unique : false, required : true, default : 0, min : 0},
  date : { type : Date, required : true, default : Date.now}
});

module.exports = mongoose.model('Redeem', redeemedSchema);

redeemedSchema.post('save', function(redeem) {
  Franchise.findById(redeem.franchise, function(err, franchise) {
    franchise.redeems.push(redeem._id);
    franchise.save();
  });
  User.findById(redeem.redeemer, function(err, user) {
    user.redeemed.push(redeem._id);
    user.save();
  });
});

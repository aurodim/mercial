var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var async = require("async");

var schema = new Schema({
  code : { type: String, unique: true, required: true},
  type : { type : String, unique : false},
  valid : { type : Boolean, unique : false, default : true},
  for : { type: Schema.Types.ObjectId, unique: false, required: true},
  expires : { type : Number, unique : false, required : true },
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

var Code = mongoose.model('Code', schema);

schema.plugin(mongooseUniqueValidator);

schema.pre('save', function(next) {
  Code.find({code : {$ne : this.code}, type : this.type, valid : true, for : this.for}, function(err, codes) {
    async.mapLimit(codes, 10, function(document, nextc){
      document.valid = false;
      document.save(nextc);
    }, function() {
      next();
    });
  });
});

module.exports = Code;

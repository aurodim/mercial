var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var addressSchema = new Schema({
  id: {type: Schema.Types.ObjectId, index: true, required: true, auto: true},
  formatted : {type : String, unique : false, required : true},
  geo : { type: [Number], default: [0,0]},
  zipcode : {type : String, unique : false, required : true}
});

addressSchema.index({geo: '2d'});

var redeemsSchema = new Schema({
  amount : {type : Number, required : true, default : 0, min : 0},
  date : {type : Date, required : true, default : Date.now}
});

var schema = new Schema({
  crypto : {type : String, required : true, unique : true},
  owner : {type : String, required : true},
  email : {type : String, required : true, unique : true},
  password : {type : String, required : true},
  name : {type : String, required : true},
  address : {type : [addressSchema], required : true, default : [], unique : false},
  website : {type : String, required : false, unique : true, index : true, sparse : true},
  manage : {
    code : {type : String, required : false, unique : false}, // Manager Code to validate manager
  },
  offers : [{type : Schema.Types.ObjectId, ref : 'Offer'}],
  reviews : [{type : Schema.Types.ObjectId, ref : 'Review'}],
  permissions : {
    notifications : {type : Boolean, required : true, default : false}
  },
  tab : {
    amount : {type : Number, required : true, default: 0, min : 0},
    redeems : [redeemsSchema]
  },
  pos: {
    type : {type: String, required: false, unique: false, index: false},
    access : {type : String, required : false, unique : false, index : false},
    identifier : {type: String, required: false, unique: false, index: false},
    data: Schema.Types.Mixed
  },
  stripe : {
    customer : {type : String, required : false, unique : true, index: true, sparse: true},
    access : {type : String, required : false, unique : true, index : true, sparse : true}
  },
  redeems : [{ type: Schema.Types.ObjectId, unique: false, required: true, ref: "Redeem" }],
  rating : {type : Number, min : 0, max : 10, required : false, default : null},
  verified : {type : Boolean, required : true, default : false},
  failed : {type : Number, default : 0, min : 0},
  locked : {type : Boolean, default : false},
  deleted : {type : Boolean, default : false},
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Business', schema, "businesses");

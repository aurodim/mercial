var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var tabsSchema = new Schema({
  amount : { type : Number, required : true, default : 0, min : 0},
  mercial : {type : Schema.Types.ObjectId, ref : 'Mercial'},
  date : { type : Date, required : true }
});

var schema = new Schema({
  crypto : {type : String, required : true, unique : true},
  fullname : {type : String, required : true},
  email : {type : String, required : true, unique : true},
  name : {type : String, required : false, unique : false},
  accesskey : {type : String, required : true, unique : true},
  mercials :  [{type : Schema.Types.ObjectId, ref : 'Mercial'}],
  stripe : {
    customer : {type : String, required : false, unique : false},
    cc : {type : String, required : false, unique : false},
    source : {type : String, required : false, unique : false}
  },
  credits: {type: Number, required: true, unique: false, default: 250, min: 0},
  tab : {
    currentTabId : {type : String, required : false, unique : false},
    owe : { type : Number, required : false, default : 0, min : 0},
    paymentFailed : { type : Boolean, required : false, unique : false, default : false},
    current : [tabsSchema],
    past : [tabsSchema]
  },
  failed : {type : Number, default : 0, min : 0},
  locked : {type : Boolean, default : false},
  deleted : {type : Boolean, default : false},
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Creator', schema);

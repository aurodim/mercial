var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var schema = new Schema({
  param : { type: String, unique: true, required: true },
  title : { type: String, unique: false, required: true },
  content :  { type: String, unique: false, required : true },
  type : [String],
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('FAQ', schema);

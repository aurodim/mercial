var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var viewsSchema = new Schema({
  mercial : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Mercial" },
  date : {type : Date, required : true},
  merchips: {type: Number, unique: false, required: false, min: 0, default: 0}
});

var transactionsSchema = new Schema({
  checkoutSession: {type: String, unique: false, required: true},
  customerID: {type: String, unique: false, required: true},
  price: {type: String, unique: false, required: true},
  products: [{type: String, unique: false, required: true}],
  date : {type : Date, required : true}
});

var schema = new Schema({
  crypto : {type : String, required : true, unique : true},
  socket : {type : String, required : true, unique : true},
  fullname : {type : String, required : true},
  email : {type : String, required : true, unique : true},
  password : {type : String, required : true},
  birthdate : {type : Date, required : false},
  merchips : {type : Number, default : 0, min : 0},
  preferences : {
    tags : [{type : String, required : false, unique : false}],
  },
  permissions : {
    notifications : {type : Boolean, required : true, default : false}
  },
  revsAnswered : [ { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Review" } ],
  views : [viewsSchema],
  redeem_validator : {type : String, required : true, unique : true},
  redeemed : [ { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Redeem" } ],
  target : {
    languages : {type : [String], required : false, unique : false},
    ethnicity : {type : String, required : false, unique : false},
    country : {type : String, required : false, unique : false},
    state : {type : String, required : false, unique : false},
    city : {type : String, required : false, unique : false},
    zipcode : {
      updated : {type : Date, required : true, default : Date.now},
      formatted : {type : String, required : false, default : null},
      geo : { type: [Number], default: [0,0]}
    }
  },
  referral : {type: String, unique: false, required: false, default: ""},
  referrer : {type: Schema.Types.ObjectId, unique: false, required: false, ref: "User"},
  awardedMerchips : {type: Number, min: 0, unique: false, required: false, default: 0},
  transactions: [transactionsSchema],
  failed : {type : Number, default : 0, min : 0},
  locked : {type : Boolean, default : false},
  deleted : {type : Boolean, default : false},
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

schema.plugin(mongooseUniqueValidator);

schema.index({"zipcode.geo" : '2d'});
module.exports = mongoose.model('User', schema);

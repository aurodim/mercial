var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var Advertiser = require('./advertiser');

var viewsSchema = new Schema({
  viewer : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "User" },
  date : {type : Date, required : true}
});

var reportsSchema = new Schema({
  moderator: { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Admin" },
  reporter : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "User" },
  reason : {type : String, unique : false, required : true},
  expand : {type : String, unique : false, required : false},
  date : {type : Date, required : true}
});

var schema = new Schema({
  _id : {type: Schema.Types.ObjectId, required: true},
  advertiser : { type: Schema.Types.ObjectId, unique: false, required: true, ref: "Creator" },
  title : { type: String, unique: false, required: true },
  video :  { type: String, unique: false, required: true },
  metadata : {
    duration : {type : Number, unique : false, required : true},
    more : [{type : String, unique : false, required : false}],
    interest : {type : Number, unique : false, default : 0, min : 0}
  },
  target : {
    languages : [{type : String, unique : false, required : false, default: 'any'}],
    ethnicities : [{type : String, unique : false, required : false, default: 'any'}],
    locationType : [{type : String, unique : false, required : false, default: 'none'}],
    locations : [{type : String, unique : false, required : false, default: 'any'}]
  },
  moderation: {
    moderated: {type: Boolean, unique: false, required: false, default: false},
    moderator: {type: Schema.Types.ObjectId, unique: false, required: false, ref: "Admin"}
  },
  views : [viewsSchema],
  viewsAmount : {type : Number, unique : false, default : 0, min : 0},
  reports : [reportsSchema],
  size :  { type: String, unique: false, required: true, default: "small" },
  age :  {
    lower : {type : Number, unique : false, default : 13, required: false},
    upper : {type : Number, unique : false, default : 99, required: false}
  },
  description :  { type: String, unique: false, default: "" },
  tags : [String],
  attributes : [String],
  merchips : {type : Number, unique : false, required : true, min : 0},
  forcedPause : {type : Boolean, default : false},
  paused : {type : Boolean, default : false},
  suspended : {type : Boolean, default : false},
  deleted : {type : Boolean, default : false},
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
}, {id : false});

schema.plugin(mongooseUniqueValidator);

schema.post('remove', function(mercial) {
  Advertiser.findById(mercial.advertiser, function(err, advertiser) {
    advertiser.mercials.pull(mercial._id);
    advertiser.save();
  });
});

module.exports = mongoose.model('Mercial', schema);

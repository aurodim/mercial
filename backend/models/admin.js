var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var actionSchema = new Schema({
  creator : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Creator" },
  mercial : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Mercial" },
  business : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Business" },
  offer : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Business" },
  admin : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Admin" },
  faq : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "FAQ" },
  user : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "User" },
  redeem : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Redeem" },
  type: { type: String, unique: false, required: true},
  created: {type : Date, required : true},
});

var schema = new Schema({
  crypto : {type : String, required : true, unique : true},
  access : {type : String, required : true, unique : true},
  password: {type: String, required: true, unique: true},
  socket : {type : String, required : true, unique : true},
  fullname : {type : String, required : true},
  email : {type : String, required : true, unique : true},
  phone : {type: String, required: true, unique: true},
  priviledge : {type : String, required : true, default : "delta"}, // delta, beta, omega
  actions : [actionSchema],
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Admin', schema);

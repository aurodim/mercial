var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var responseSchema = new Schema({
  respondent: { type: Schema.Types.ObjectId, unique: false, required: true, ref: "User" },
  rating: {type: Number, unique: false, min: 0, required:false},
  experience: {type: String, unique: false, required: false},
  recommend: {type: Boolean, unique: false, required: false},
  recExplain: {type: String, unique: false, required: false},
  addition: {type: String, unique: false, required: false},
  date: {type: Date, required: true, default: Date.now}
});

var reviewSchema = new Schema({
  creatorType: {type: String, required: true, unique: false},
  business: { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Business" },
  creator: { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Creator" },
  image: {type: String, unique: false, required: true},
  responses: [responseSchema],
  name: {type: String, unique: false, required: true},
  description: {type: String, unique: false, required: true},
  questions: [{type: String, unique: false, required: true}],
  merchips: {type: Number, unique: false, required: false},
  created : { type : Date, required : true, default : Date.now},
  updated : { type : Date, required : true, default : Date.now},
  deleted: {type: Boolean, required: true, default: false}
});

module.exports = mongoose.model('Review', reviewSchema);

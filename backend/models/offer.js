var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var Franchise = require('./franchise');

var locationSchema = new Schema({
  formatted : {type : String, unique : false, required : true},
  geo : { type: [Number], default: [0,0]}
});

locationSchema.index({geo: '2d'});

var reportsSchema = new Schema({
  moderator: { type: Schema.Types.ObjectId, unique: false, required: false, ref: "Admin" },
  reporter : { type: Schema.Types.ObjectId, unique: false, required: false, ref: "User" },
  reason : {type : String, unique : false, required : true},
  expand : {type : String, unique : false, required : false},
  date : {type : Date, required : true}
});

var schema = new Schema({
  _id : {type : Schema.Types.ObjectId, required : true, auto : false},
  franchise : {type : Schema.Types.ObjectId, ref : 'Business'},
  name : { type : String, unique : false, required : true},
  description : { type : String, unique : false, required : false },
  image : { type : String, unique : false, required : true },
  locations : [locationSchema],
  reports : [reportsSchema],
  upc: {type: Number, unique: false, required: false, min: 0, default: null},
  redeem_code : { type : String, unique : true, required : true },
  redeemsAmount : { type : Number, unique : false, required : false, default : 0, min : 0},
  merchips : { type : Number, unique : false, required : true, min : 0},
  savings : { type : Number, unique : false, required : true, min : 0, default: 0},
  refund : { type : Number, unique : false, required : true, min : 0, default: 0},
  paused : { type: Boolean, unique: false, required: false, default: false},
  suspended : {type : Boolean, default : false},
  deleted : {type : Boolean, default : false},
  moderation: {
    moderated: {type: Boolean, unique: false, required: false, default: false},
    moderator: {type: Schema.Types.ObjectId, unique: false, required: false, ref: "Admin"}
  },
  created : {type : Date, required : true},
  updated : {type : Date, required : true, default : Date.now}
}, { id: false });

schema.plugin(mongooseUniqueValidator);

schema.post('remove', function(offer) {
  Franchise.findById(offer.franchise, function(err, franchise) {
    franchise.offers.pull(offer._id);
    franchise.save();
  });
});

module.exports = mongoose.model('Offer', schema);

const Offer = require('../models/offer.js');
const qr = require('qr-image');
const mongoose = require('mongoose');
const cloudinary = require('cloudinary'),
      fs = require('fs'),
      path = require("path");
const formula = require("./formula.js");
const mailer = require("./mailer.js");
const moment = require('moment');

var exports = {};
exports.all = All;
exports.of = Of;
exports.one = One;
exports.new = New;
exports.update = Update;
exports.remove = Remove;
exports.addReport = AddReport;
exports.addRating = AddRating;
exports.formulateFastRedeemCode = FormulateRedeemCodeFast;
module.exports = exports;

cloudinary.config({
  cloud_name: 'aurodim',
  api_key: process.env.CLOUDINARY_PUBLIC,
  api_secret: process.env.CLOUDINARY_PRIVATE
});

function All(query, service = 'f', limit = 100, skip = 0, sort = {created : -1}, lang = "en") {
  return new Promise((resolve, reject) => {
    Offer.find(query).skip(skip).sort(sort).limit(limit).populate('franchise').exec(function(error, offers) {
      if (error) return reject(error);

      FormulateJSON(offers, service, false, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}

function Of(query, service = 'f', lang = "en") {
  return new Promise((resolve, reject) => {
    Offer.find(query).sort({created : -1}).populate('franchise').exec(function(error, offers) {
      if (error) return reject(error);

      FormulateJSON(offers, service, false, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}

function One(query, service = 'f', raw = false, lang = "en") {
  return new Promise((resolve, reject) => {
    Offer.findOne(query).populate('franchise').exec(function(error, offer) {
      if (error) return reject(error);

      if (!offer) {
        return resolve(false);
      }

      if (raw) {
        return resolve(offer);
      }

      FormulateJSON([offer], service, true, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}

function New(franchise, data, file) {
  var locations = [];
  let submittedLocations = JSON.parse(data.locations).filter(loc => loc.c).map(loc => loc.i);
  for (let location of franchise.address) {
    if (submittedLocations.includes(location._id.toString())) {
      let loc = {
          formatted : location.formatted,
          geo : location.geo
        };

        locations.push(loc);
    }
  }

  return new Promise((resolve, reject) => {
    const id = mongoose.Types.ObjectId();
    let image = "";
    let redeem_qr = "";
    let thumbnail = "";
    FormulateRedeemCode(franchise._id, id).then((qrc)=>{
      redeem_qr = qrc;
      return FormulateImage(file, franchise._id, id);
    }).then((image)=>{
      image = image;

      const random = Math.floor(Math.random() * 6);
      Admin.findOne().skip(random).exec(function (err, admin) {
        if (error) return reject(error);

        var offer = new Offer({
          _id : id,
          franchise : franchise._id,
          name : data.title,
          description : data.description,
          image : image,
          upc : data.upc && data.upc.length == 12 ? parseInt(data.upc) : null,
          locations : locations,
          redeem_code : redeem_qr,
          moderation: {
            moderator: admin._id
          },
          savings : parseFloat(data.savings.replace(/[^0-9.]/g, "")),
          refund : parseFloat(data.refund.replace(/[^0-9.]/g, "")),
          merchips : formula.getOfferPrice(parseFloat(data.refund.replace(/[^0-9.]/g, ""))),
          created : moment().toDate(),
        });

        offer.save(function(error, doc) {

          if (error) return reject(error);

          FormulateJSON([doc], 'f', true).then((json) => resolve(json)).catch((error) => reject(error));
        });
      });
    }).catch((error)=>{
      reject(error);
    });
  });
}

function Update(data, franchise = null) {
  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    query.suspended = false;
    query.deleted = false;
    if (franchise) {
      query.franchise = franchise;
    }
    One(query, 'f', true).then((offer) => {
      var locations = [];
      let submittedLocations = JSON.parse(data.locations).filter(loc => loc.c).map(loc => loc.i);
      for (let location of franchise.address) {
        if (submittedLocations.includes(location._id.toString())) {
          let loc = {
              formatted : location.formatted,
              geo : location.geo
            };

            locations.push(loc);
        }
      }

      offer.name = data.title;
      offer.description = data.description;
      offer.locations = locations;
      offer.upc = data.upc && data.upc.length == 12 ? parseInt(data.upc) : null;
      offer.savings = parseFloat(data.savings.replace(/[^0-9.]/g, ""));
      offer.refund = parseFloat(data.refund.replace(/[^0-9.]/g, ""));
      offer.merchips = formula.getOfferPrice(offer.refund);
      offer.updated = moment().toDate();

      offer.save(function(error, doc) {
        if (error) return reject(error);

        let cbd = {
          id : doc._id
        };

        resolve(cbd);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function Remove(data, franchise = null) {
  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    if (franchise) {
      query.franchise = franchise._id;
    }
    One(query, 'f', true).then((offer) => {
      offer.deleted = true;
      offer.save(function(error) {
        if (error) return reject(error);
      });

      cloudinary.v2.uploader.destroy(offer.image,
      { resource_type: "image" },
      function(error, result) { });

      let cbd = {};

      resolve(cbd);
    });
  });
}

function AddReport(data, user) {
  const id = data.id;
  const reason = data.id;
  const expand = data.id;

  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    query.deleted = false;
    query.suspended = false;
    One(query, 'f', true).then((offer) => {

      offer.reports.push({
        reporter : user,
        reason : reason,
        expand : expand,
        date : moment().toDate()
      });

      if (offer.reports > 8) {
        mailer.suspendedOffer(offer.franchise.email, offer);
        offer.suspended = true;
      }

      offer.save((error) => {
        if (error) return reject(error);

        resolve(offer);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function AddRating(data, user) {
  const id = data.id;
  const rating = data.rating;

  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    query.deleted = false;
    query.suspended = false;
    One(query, 'f', true).then((offer) => {

      if (!offer.franchise.rating) {
        offer.franchise.rating = parseFloat(rating);
      } else {
        offer.franchise.rating = (offer.franchise.rating + parseFloat(rating)) / 2;
      }
      offer.franchise.save((error) => {
        if (error) return reject(error);

        resolve(rating);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function FormulateImage(file, franchise, id) {
  return new Promise((resolve, reject) => {
    const filepath = file.path;
    cloudinary.v2.uploader.upload(filepath, {folder : `${process.env.CLOUDINARY_FOLDER}/businesses/${franchise}/${id}`}, function(error, result) {
      if (error) return reject(error);
      fs.unlink(filepath, (err) => {});
      resolve(result.secure_url);
    });
  });
}

function FormulateRedeemCode(franchise, id) {
  return new Promise((resolve, reject) => {
    const imagepath = path.join(process.cwd(), `tmp/qr_${id}_${franchise}.svg`);
    var redeem_qr = qr.image(`_i:${id};_f:${franchise};_d:${moment().valueOf()};;`, { type: 'svg' });
    redeem_qr.pipe(fs.createWriteStream(imagepath)).on('finish', () => {
      cloudinary.v2.uploader.upload(imagepath, {folder : `${process.env.CLOUDINARY_FOLDER}/businesses/${franchise}/${id}`}, function(error, result) {
        if (error) return reject(error);
        fs.unlink(imagepath, () => {});
        resolve(result.secure_url);
      });
    });
  });
}

// @Overloaded method for fomulating redeem codes
function FormulateRedeemCodeFast(franchise, id, userRedeemCode) {
  return new Promise((resolve, reject) => {
    const DIR = path.join(process.cwd(), '/tmp');
    if (!fs.existsSync(DIR)){
        fs.mkdirSync(DIR);
    }
    const imagepath = path.join(process.cwd(), `tmp/qr_${id}_${franchise}_d_${Math.random() * 99}_userfastqr.svg`);
    var redeem_qr = qr.image(`_i:${id};_f:${franchise};_d:${moment().valueOf()};_urc:${userRedeemCode};;`, { type: 'svg' });
    redeem_qr.pipe(fs.createWriteStream(imagepath)).on('finish', (error) => {
      resolve(imagepath);
    });
  });
}

function FormulateJSON(arr, service, one = false, lang = 'en') {
  let promises = [];

  for (let offer of arr) {
    let element = {};
    let locations = [];

    for (let location of offer.locations) {
      locations.push({
        formatted : location.formatted,
        lat : location.geo[1],
        lng : location.geo[0]
      });
    }
    element.id = offer._id;
    element.franchise = {};
    if (offer.franchise) {
      element.franchise.id = offer.franchise._id;
      element.franchise.name = offer.franchise.name;
    } else {
      element.franchise.id = null;
      element.franchise.name = null;
    }
    element.name = offer.name;
    element.paused = offer.paused;
    element.description = offer.description;
    if (service != 'f') {
      element.image = offer.image;
    } else {
      element.image = offer.image.split(/upload\/v[0-9]*\//)[1];
    }
    if (service == 'a') {
      element.reports = offer.reports;
      element.refund = offer.refund;
    }
    if (service == 'r') {
      element.refund = `$${offer.refund}`;
      element.upc = offer.upc;
    }
    element.redeem_code = offer.redeem_code;
    element.locations = locations;
    element.website = offer.website || null;
    element.merchips = offer.merchips;
    element.savings = `$${offer.savings.toFixed(2)}`;

    promises.push(translate(element, lang));
  }

  return Promise.all(promises).then((data) => {
    if (one) return data[0];

    return data;
  });
}


function translate(element, lang) {
  return new Promise((resolve, reject) => {
    if (lang == "en") resolve(element);

    // googleTranslate.translate([element.name, element.description], lang, (err, res) => {
    //   if (err) return resolve(element);
    //     element.name = res[0].translatedText;
    //     element.description = res[1].translatedText;
    //     resolve(element);
    // });
    resolve(element);
  });
}

const Review = require('../models/review.js');
const mongoose = require('mongoose');
const moment = require('moment');
const cloudinary = require('cloudinary'),
      fs = require('fs'),
      path = require("path");
const formula = require("./formula.js");
const mailer = require("./mailer.js");

var exports = {};
exports.all = All;
exports.of = Of;
exports.one = One;
exports.new = New;
exports.update = Update;
exports.remove = Remove;
exports.answered = Answered;
module.exports = exports;

cloudinary.config({
  cloud_name: 'aurodim',
  api_key: process.env.CLOUDINARY_PUBLIC,
  api_secret: process.env.CLOUDINARY_PRIVATE
});

function All(query, service = 'fa', limit = 100, skip = 0, sort = {created : -1}, lang = "en") {
  return new Promise((resolve, reject) => {
    Review.find(query).skip(skip).sort(sort).limit(limit).populate('creator').populate("business").exec(function(error, reviews) {
      if (error) return reject(error);

      FormulateJSON(reviews, service, false, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}

function Of(query, service = 'f', lang = "en") {
  return new Promise((resolve, reject) => {
    Review.find(query).sort({created : -1}).populate('creator').populate("business").exec(function(error, reviews) {
      if (error) return reject(error);

      FormulateJSON(reviews, service, false, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}

function One(query, service = 'f', raw = false, lang = "en") {
  return new Promise((resolve, reject) => {
    Review.findOne(query).populate('creator').populate("business").exec(function(error, review) {
      if (error) return reject(error);

      if (!review) {
        return resolve(false);
      }

      if (raw) {
        return resolve(review);
      }

      FormulateJSON([review], service, true, lang).then((json) => resolve(json)).catch((error) => reject(error));;
    });
  });
}


function New(type, account, data, file) {
  return new Promise((resolve, reject) => {
    const id = mongoose.Types.ObjectId();
    let image = "";
    FormulateImage(file, account._id, id).then((image)=>{
      image = image;

      var review = new Review({
        _id : id,
        creatorType: type,
        business : account._id,
        name : data.title,
        description : data.description,
        image : image,
        merchips : formula.reviewMerchips(data.questions),
        questions: data.questions.split("*QU*"),
        created : moment().toDate()
      });

      review.save(function(error, doc) {
        if (error) return reject(error);

        FormulateJSON([doc], 'f', true).then((json) => resolve(json)).catch((error) => reject(error));
      });
    }).catch((error)=>{
      reject(error);
    });
  });
}

function Answered(id, user, data) {
  return new Promise((resolve, reject) => {
    One({_id: id}, 'u', true).then((review) => {

      review.responses.push({
        respondent: user._id,
        merchips: review.merchips,
        rating: data.rating,
        experience: data.rating,
        recommend: data.recommend,
        recExplain: data.recExplain,
        addition: data.addition,
        date: moment().toDate()
      });

      review.save((error, doc) => {
        if (error) return reject(error);

        resolve(doc);
      });
    }).catch(error => {
      reject(error);
    })
  });
}

function Update(data, type, account = null) {
  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    query.deleted = false;
    if (account) {
      query.creatorType = type;
      query.business = account;
    }
    One(query, 'f', true).then((review) => {
      if (!review) {
        reject("no_review");
      }
      review.name = data.title;
      review.description = data.description;
      review.updated = moment().toDate();

      review.save(function(error, doc) {
        if (error) return reject(error);

        let cbd = {
          id : doc._id
        };

        resolve(cbd);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function Remove(data, account = null) {
  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    if (account) {
      query.business = account._id;
    }
    One(query, 'f', true).then((review) => {
      review.deleted = true;
      review.save(function(error) {
        if (error) return reject(error);
      });

      cloudinary.v2.uploader.destroy(review.image,
      { resource_type: "image" },
      function(error, result) { });

      let cbd = {};

      resolve(cbd);
    });
  });
}

function FormulateImage(file, account, id, type = 'businesses') {
  return new Promise((resolve, reject) => {
    const filepath = file.path;
    cloudinary.v2.uploader.upload(filepath, {folder : `${process.env.CLOUDINARY_FOLDER}/${type}/${account}/${id}`}, function(error, result) {
      if (error) return reject(error);
      fs.unlink(filepath, (err) => {});
      resolve(result.secure_url);
    });
  });
}

function FormulateJSON(arr, service, one = false, lang = 'en') {
  let promises = [];

  for (let review of arr) {
    let element = {};

    element.id = review._id;
    element.account = {};
    element.account.id = review.business._id;
    element.account.name = review.business.name;
    element.name = review.name;
    element.description = review.description;
    if (service != 'f') {
      element.image = review.image;
    } else {
      element.image = review.image.split(/upload\/v[0-9]*\//)[1];
    }
    element.merchips = review.merchips;
    element.questions = review.questions;

    promises.push(translate(element, lang));
  }

  return Promise.all(promises).then((data) => {
    if (one) return data[0];

    return data;
  });
}

function translate(element, lang) {
  return new Promise((resolve, reject) => {
    if (lang == "en") resolve(element);

    resolve(element);
  });
}

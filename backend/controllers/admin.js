var liana = require('forest-express-mongoose');
const Users = require('../controllers/users.js');
const Franchises = require('../controllers/franchises.js');
const Advertisers = require('../controllers/advertisers.js');
const Mercials = require('../controllers/mercials.js');
const Offers = require('../controllers/offers.js');
const Mailer = require('../controllers/mailer.js');
const Mercial = require('../models/mercial.js');
const Admin = require('../models/admin.js');
const Offer = require('../models/offer.js');
const Redeem = require('../models/redeem.js');
const moment = require('moment');
const Cryptr = require('cryptr');
const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_AUTH;
const client = require('twilio')(accountSid, authToken);
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

var exports = {};
exports.toggleMerchips = ToggleMerchips;
exports.moderateMercial = ModerateMercial;
exports.merchipsBasedRevenue = MerchipsBasedRevenue;
exports.projectedAnnualRevenue = ProjectedAnnualRevenue;
exports.userSecondsViewed = UserSecondsViewed;
exports.advertisersViews = AdvertisersViews;
exports.addCredits = AddCredits;
exports.loginAttempt = LoginAttempt;
exports.login = Login;
exports.dashboardData = DashboardData;
exports.offersData = OffersData;
exports.mercialsData = MercialsData;
exports.reportsData = ReportsData;
exports.rejectOffer = RejectOffer;
exports.rejectVideo = RejectVideo;
exports.approveOffer = ApproveOffer;
exports.approveVideo = ApproveVideo;
module.exports = exports;

function ModerateMercial(req, res) {
  let mercialID = req.body.data.attributes.ids[0];
  let reject = req.body.data.attributes.values.remove;
  return Promise.resolve(Mercials.one({_id: userID}, 'a', true).then((mercial) => {

    mercial.moderation.moderated = true;
    mercial.forcedPause = reject;
    if (reject) {
      Mailer.suspendedMercial(mercial.advertiser, mercial, "moderator_did_not_approve").then(() => {});
    }
    mercial.save((error) => {
      if (error) throw new Error('Database Error');

      res.send({ success: 'Moderated!' })
    });
  }).catch((error) => {
    res.status(400).send({ error: error });
  }));
}

function ToggleMerchips(req, res) {
  let userID = req.body.data.attributes.ids[0];
  let amount = req.body.data.attributes.values.amount;
  let remove = req.body.data.attributes.values.remove;
    return Promise.resolve(Users.one({_id: userID}, null).then((user) => {

    if (remove) {
      user.merchips = user.merchips >= amount ? user.merchips - amount : 0;
    } else {
      user.merchips += amount;
    }
    user.save((error, doc) => {
      if (error) throw new Error('_dberror');

      res.send({ success: 'Toggled user merchips!' })
    });
  }).catch((error) => {
    res.status(400).send({ error: error });
  }));
}

function AddCredits(req, res) {
  let creatorID = req.body.data.attributes.ids[0];
  let amount = req.body.data.attributes.values.amount;
    return Promise.resolve(Advertisers.one({_id: creatorID}, null).then((advertiser) => {

    advertiser.credits += amount;
    advertiser.save((error, doc) => {
      if (error) throw new Error('_dberror');

      res.send({ success: 'Added credits to creator\'s account!' })
    });
  }).catch((error) => {
    res.status(400).send({ error: error });
  }));
}

function MerchipsBasedRevenue(req, res, asFunction = false) {
  return Promise.resolve(Users.all({}, null).then((users) => {

    let mic = 0;
    for (let user of users) {
      mic += user.merchips;
    }

    let result = 0.0025 * mic;

    let json = new liana.StatSerializer({ value: result }).perform();
    res.send(json);
    return Promise.resolve(result);
  }).catch((error) => {
    res.send(null);
    return Promise.resolve(0);
  }));
}

/* jshint ignore:start */

function ProjectedAnnualRevenue(req, res) {
  MerchipsBasedRevenue(null, {send : function() {}}, true).then((mbr) => {
    let d = moment("2019-01-04").valueOf();
    let nowD = moment().valueOf();
    let span = Math.floor((nowD - d) / 3600000 / 24);
    let result = mbr * (365 / span) * (1000 / 25);
    let json = new liana.StatSerializer({ value: result }).perform();
    res.send(json);
  });
}

/* jshint ignore:end */

function UserSecondsViewed(req, res) {
  Users.one({_id : req.body.record_id}).then((user) => {

    let totalSeconds = 0;

    for (let view of user.views) {
      totalSeconds += view.mercial.metadata.duration;
    }

    let json = new liana.StatSerializer({ value: totalSeconds }).perform();
    res.send(json);
  }).catch((error) => {
    res.send(null);
  });
}

function AdvertisersViews(req, res) {
  let query = {};
  if (req.body.record_id) {
    query = {_id : req.body.record_id};
  }

  Advertisers.all(query).then((advertisers) => {
    let totalViews = 0;

    for (let advertiser of advertisers) {
      for (let mercial of advertiser.mercials) {
        totalViews += mercial.views.length;
      }
    }

    let json = new liana.StatSerializer({ value: totalViews }).perform();
    res.send(json);
  }).catch((error) => res.send(null));
}

function LoginAttempt(body, callback) {
  const key = body.key;
  const encryptKey = crypto.createCipher('aes-128-cbc', key);
  let encryptedKey = encryptKey.update(process.env.ADMIN_ACCESS_SECRET, 'utf8', 'hex');
  encryptedKey += encryptKey.final('hex');

  Admin.findOne({access : encryptedKey}, function(error, admin) {
    console.log(error);
    if (error) return callback({"code" : 500}, null);

    console.log(admin);

    if (!admin) return callback(null, {_m : "if fit, continue", _tk : null});

    const access = crypto.randomBytes(4).toString("hex")
    let hashedPassword = bcrypt.hashSync(access, 11);

    admin.password = hashedPassword;
    admin.save();

    client.messages.create({
       body: `MER ACCESS: ${access}`,
       from: process.env.TWILIO_NUMBER,
       to: admin.phone
     }).then(message => console.log(message.sid)).catch(error => {
       console.log(error);
     });

    callback(null, {
      _m : "if fit, continue"
    });
  });
}

function Login(body, callback) {
  const key = body.key;
  const encryptKey = crypto.createCipher('aes-128-cbc', key);
  let encryptedKey = encryptKey.update(process.env.ADMIN_ACCESS_SECRET, 'utf8', 'hex')
  encryptedKey += encryptKey.final('hex');

  const password = body.password;

  Admin.findOne({access : encryptedKey}, function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback(null, {_m : "_not_admin", _tk : null});

    if (!bcrypt.compareSync(password, admin.password)) {
      return callback(null, {_m : "_passinvalid", _tk : null});
    }

    let hashedPassword = bcrypt.hashSync(crypto.randomBytes(8).toString("hex"), 11);

    admin.password = hashedPassword;
    admin.actions.push({
      type: 'login',
      created: moment().toDate()
    });
    admin.save();

    var token = jwt.sign({_tk : admin.crypto, _fn : admin.fullname, _s : admin.socket}, process.env.JWT_M_SECRET, {expiresIn : 86400 * 7});

    callback(null, {
      _m : "_success_",
      _tk : token,
      _fn : admin.fullname
    });
  });
}

function DashboardData(auth, callback) {
  var aat = jwt.decode(auth);
  Admin.findOne({crypto : aat._tk}).exec(async function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    const [ oL, mL, rL ] = await Promise.all([
      Offer.countDocuments({}, function(error, offersCount) {
        return error ? null : offersCount;
      }),
      Mercial.countDocuments({}, function(error, mercialsCount) {
        return error ? null : mercialsCount;
      }),
      Redeem.countDocuments({}, function(error, redeemsCount) {
        return error ? null : redeemsCount;
      })
    ]);

    callback(null, {
      _m : "API returned dashboard data successfully",
      _tk : auth,
      _d : {
        o : oL,
        m : mL,
        r : rL
      }
    });
  });
}

function OffersData(auth, callback) {
  var aat = jwt.decode(auth);
  Admin.findOne({crypto : aat._tk}).exec(function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Offers.all({"moderation.moderated": false, "moderation.moderator": admin._id}, 'a').then(offers => {
      callback(null, {
        _m : "API returned offers succesfully",
        _tk : auth,
        _d : {
          o : offers,
        }
      });
    }).catch(error => {
      callback(null, {
        _m : "API error",
        _tk : auth,
        _d : {
          o : null
        }
      });
    });
  });
}

function MercialsData(auth, callback) {
  var aat = jwt.decode(auth);
  Admin.findOne({crypto : aat._tk}).exec(function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Mercials.all({"moderation.moderated": false, "moderation.moderator": admin._id}, 'a').then(mercials => {
      callback(null, {
        _m : "API returned videos succesfully",
        _tk : auth,
        _d : {
          m : mercials,
        }
      });
    }).catch(error => {
      callback(null, {
        _m : "API error",
        _tk : auth,
        _d : {
          m : null
        }
      });
    });
  });
}

function ReportsData(auth, callback) {
  var aat = jwt.decode(auth);
  Admin.findOne({crypto : aat._tk}).exec(function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);
    let reports = [];

    Offers.all({deleted: false, "reports.0": { $exists : true }}, 'a').then(offers => {

      for (let report of offers) {
        reports.push({
          type: 'offer',
          offer: report,
          reports: report.reports
        });
      }

      Mercials.all({deleted: false, "reports.0": {$exists : true}}, 'a').then(mercials => {
        for (let report of mercials) {
          reports.push({
            type: 'mercial',
            mercial: report,
            reports: report.reports
          });
        }

        callback(null, {
          _m : "API returned offers succesfully",
          _tk : auth,
          _d : {
            r : reports,
          }
        });
      }).catch((error) => {
        console.log(error);
        callback(null, {
          _m : "API error",
          _tk : auth,
          _d : {
            r : null
          }
        });
      });
    }).catch(error => {
      console.log(error);
      callback(null, {
        _m : "API error",
        _tk : auth,
        _d : {
          r : null
        }
      });
    });
  });
}

function RejectOffer(body, callback) {
  const auth = body.auth;
  const id = body.id;
  const reason = body.reason;
  const expand = body.expand;

  var aat = jwt.decode(auth);

  Admin.findOne({crypto : aat._tk}, function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Offer.findOne({_id: id}, function(error, offer) {
      offer.suspended = true;
      offer.moderation.moderated = true;
      offer.reports.push({
        moderator: admin._id,
        reason : reason,
        expand : expand,
        date : moment().toDate()
      });
      offer.updated = moment().toDate();

      offer.save((error) => {
        if (error) {
          return callback({"code" : 500}, null);
        }

        callback(null, {
          _m : "Rejected offer successfully",
          _tk : auth,
          _d : {
            r : null
          }
        });
      });
    });
  });
}

function RejectVideo(body, callback) {
  const auth = body.auth;
  const id = body.id;
  const reason = body.reason;
  const expand = body.expand;

  var aat = jwt.decode(auth);

  Admin.findOne({crypto : aat._tk}, function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Mercial.findOne({_id: id}, function(error, mercial) {
      mercial.suspended = true;
      mercial.moderation.moderated = true;
      mercial.reports.push({
        moderator: admin._id,
        reason : reason,
        expand : expand,
        date : moment().toDate()
      });
      mercial.updated = moment().toDate();

      mercial.save((error) => {
        if (error) {
          return callback({"code" : 500}, null);
        }

        callback(null, {
          _m : "Rejected mercial successfully",
          _tk : auth,
          _d : {
            r : null
          }
        });
      });
    });
  });
}

function ApproveOffer(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var aat = jwt.decode(auth);

  Admin.findOne({crypto : aat._tk}, function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Offer.findOne({_id: id}, function(error, offer) {
      offer.moderation.moderated = true;
      offer.updated = moment().toDate();

      offer.save((error) => {
        if (error) {
          return callback({"code" : 500}, null);
        }

        callback(null, {
          _m : "Approved offer successfully",
          _tk : auth,
          _d : {
            r : null
          }
        });
      });
    });
  });
}

function ApproveVideo(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var aat = jwt.decode(auth);

  Admin.findOne({crypto : aat._tk}, function(error, admin) {
    if (error) return callback({"code" : 500}, null);

    if (!admin) return callback({"code" : 401}, null);

    Mercial.findOne({_id: id}, function(error, mercial) {
      mercial.moderation.moderated = true;
      mercial.updated = moment().toDate();

      mercial.save((error) => {
        if (error) {
          return callback({"code" : 500}, null);
        }

        callback(null, {
          _m : "Approved mercial successfully",
          _tk : auth,
          _d : {
            r : null
          }
        });
      });
    });
  });
}

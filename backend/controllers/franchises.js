const Franchise = require('../models/franchise.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const Offers = require("./offers");
const Analyzer = require("./analyzer.js");
const FAQS = require("./faqs.js");
const Codes = require("./codes.js");
const Mailer = require("./mailer.js");
const Redeems = require("./redeems.js");
const Reviews = require("./reviews.js");
const mongoose = require("mongoose");
var stripe = require("stripe")(process.env.STRIPE_SECRET);
const qr = require('qr-image');
const cloudinary = require('cloudinary'),
      fs = require('fs'),
      path = require("path");
const moment = require('moment');
const scheduler = require("./scheduler");
const request = require('request');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.POS_ACCESS_SECRET);

var exports = {};
exports.createFranchise = CreateFranchise;
exports.loginFranchise = LoginFranchise;
exports.forgot = ForgotPassword;
exports.reset = ResetPassword;
exports.deleteAccount = DeleteAccount;
exports.analyticsData = AnalyticsData;
exports.helpData = HelpData;
exports.locationsData = LocationsData;
exports.uploadOffer = UploadOffer;
exports.updateOffer = UpdateOffer;
exports.deleteOffer = DeleteOffer;
exports.offerAnalyticsData = OfferAnalyticsData;
exports.offersData = OffersData;
exports.offersDataManager = OffersDataManager;
exports.editorData = EditorData;
exports.pauseOffer = PauseOffer;
exports.uploadReview = UploadReview;
exports.updateReview = UpdateReview;
exports.deleteReview = DeleteReview;
exports.reviewsData = ReviewsData;
exports.reviewAnalyticsData = ReviewAnalyticsData;
exports.editorReviewData = EditorReviewsData;
exports.redeemsData = RedeemsData;
exports.settingsData = SettingsData;
exports.updateAccount = UpdateAccount;
exports.updateFranchise = UpdateFranchise;
exports.verifyClover = VerifyClover;
exports.verifySquare = VerifySquare;
exports.verifyStripe = VerifyStripe;
exports.updateLocation = UpdateLocation;
exports.updateManagers = UpdateManagers;
exports.updatePos = UpdatePos;
exports.updateTab = UpdateTab;
exports.redeem = RedeemTab;
exports.updatePrivacy = UpdatePrivacy;
exports.one = One;
module.exports = exports;

cloudinary.config({
  cloud_name: 'aurodim',
  api_key: process.env.CLOUDINARY_PUBLIC,
  api_secret: process.env.CLOUDINARY_PRIVATE
});

function One(query) {
  return new Promise((resolve, reject) => {
    Franchise.findOne(query).exec(function(error, franchise) {
      if (error) reject(error);

      resolve(franchise);
    });
  });
}

function CreateFranchise(body, callback) {
  const fullname = body.fullname;
  const email = body.email.toLowerCase().trim();
  const password = body.password;
  const name = body["franchise-name"].trim();
  const ip = body.ip;
  const address = {
    formatted : body["franchise-address"],
    geo : [body["franchise-lng"], body["franchise-lat"]],
    zipcode : body["franchise-postal"]
  };
  const country = body["franchise-country"];

  if (password.length < 4) {
    return callback(null, {_m : "_passshort", _tk : null});
  }

  var hashedPassword = bcrypt.hashSync(password, 12);
  Franchise.findOne({email : email}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (franchise) return callback(null, {_m : "_emailinuse", _tk : null});

    const key = crypto.randomBytes(9).toString("hex") + "." + crypto.randomBytes(3).toString("hex") + "." + crypto.randomBytes(6).toString("hex");

    var newFranchise = new Franchise({
      crypto : key,
      owner : fullname,
      email : email,
      password : hashedPassword,
      name : name,
      address : [address],
      created : moment().toDate(),
    });

    let unix = moment().unix();

    newFranchise.save(function(error, doc) {
      if (error) {return callback({"code" : 500}, null);}

      Mailer.welcomeBusiness(doc).then(() => {
        var token = jwt.sign({_tk : key, _ffn : name}, process.env.JWT_F_SECRET, {expiresIn : 86400 * 365});

        callback(null, {
          _m : "_success",
          _tk : token,
          _ffn : name
        });
      }).catch((error) => {
        callback({"code" : 500}, null)
      });
    });
  });
}

function LoginFranchise(body, callback) {
  const email = body.email.toLowerCase().trim();
  const password = body.password;

  Franchise.findOne({email : email, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback(null, {_m : "_emailinvalid", _tk : null});

    if (franchise.locked) {
      return callback(null, {_m : "_accountlocked", _tk : null});
    }

    if (!bcrypt.compareSync(password, franchise.password)) {
      franchise.failed = franchise.failed + 1;
      if (franchise.failed >= 5) {
        franchise.locked = true;
      }
      franchise.save();
      return callback(null, {_m : "_passinvalid", _tk : null});
    }

    franchise.failed = 0;
    franchise.deleted = false;
    franchise.save();

    var token = jwt.sign({_tk : franchise.crypto, _ffn : franchise.name}, process.env.JWT_F_SECRET, {expiresIn : 86400 * 365});

    callback(null, {
      _m : "Successfully logged in",
      _tk : token,
      _ffn : franchise.name
    });
  });
}

function ForgotPassword(body, callback) {
  const email = body.email.toLowerCase().trim();
  const franchise = body.franchise;

  Franchise.findOne({email : email, name : franchise, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback(null, {_m : "_mismatch", _tk : null});

    let length = Math.floor(Math.random() * (4 - 3 + 1)) + 3;
    const code = crypto.randomBytes(length).toString("hex");
    Codes.new({owner : franchise._id, type : 'reset', code : code, days : 1}).then(() => Mailer.resetCode(franchise, code, true)).then((res) => {
      callback(null, {
        _m : "Successfully sent code"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ResetPassword(body, callback) {
  const email = body.email.toLowerCase().trim();
  const code = body.code;
  const password = body["new-password"];

  if (password.length < 4) {
    return callback(null, {_m : "_passshort", _tk : null});
  }

  Franchise.findOne({email : email, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback(null, {_m : "_mismatch", _tk : null});

    Codes.verify({owner : franchise._id, type : 'reset', code : code}).then((code) => {
      if (code) {
        var hashedPassword = bcrypt.hashSync(password, 12);
        franchise.password = hashedPassword;
        franchise.save(function(error) {
          Codes.invalidate(code).then(()=>{
            callback(null, {
              _m : "_success"
            });
          });
        });
      } else {
        callback(null, {
          _m : "_invalidcode"
        });
      }
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function DeleteAccount(body, callback) {
  const auth = body.auth;
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}).exec(function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    franchise.deleted = true;
    franchise.updated = moment().toDate();

    franchise.save(function(error) {
      if (error) return callback({"code" : 500}, null);

      scheduler.deleteAccount(advertiser, "business");

      callback(null, {
        _m : "API deleted franchise account successfully",
        _tk : body.auth,
        _d : null
      });
    });
  });
}


function AnalyticsData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}).populate("redeems").populate("offers").exec(function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Analyzer.franchiseDashboard(franchise).then((data) => {
      callback(null, {
        _m : "API returned analyics data successfully",
        _tk : auth,
        _d : data
      });
    }).catch((error) => callback({"code" : 500}, null));
  });
}

function HelpData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    FAQS.all('f', ['franchise']).then((faqs) => {
      callback(null, {
        _m : "API returned help data successfully",
        _tk : auth,
        _d : {
          _f : faqs
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function LocationsData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    let locations = [];

    for (let location of franchise.address) {
      let obj = {
        "i" : location._id,
        "f" : location.formatted,
        "c" : false
      };
      locations.push(obj);
    }

    let data = {
      "_l" : locations
    };

    callback(null, {
      _m : "API returned locations data successfully",
      _tk : auth,
      _d : data
    });
  });
}

function OffersData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Offers.of({franchise : franchise._id, suspended : false, deleted : false}).then((offers) => {

      callback(null, {
        _m : "API returned offers data successfully",
        _tk : auth,
        _d : {
          _o : offers
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function OffersDataManager(query, callback) {
  const id = query.identifier;
  const manager = query.manager;
  Franchise.findOne({_id : id, 'manage.code' : manager, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Offers.of({franchise : franchise._id, suspended : false, deleted : false}).then((offers) => {

      if (franchise.pos.type == 'clover' && franchise.pos.access) {
        return request.get(`${process.env.CLOVER_API_ENDPOINT}/v3/merchants/${franchise.pos.data.merchant_id}/employees?access_token=${cryptr.decrypt(franchise.pos.access)}`, {}, function(error, res, body) {
          let deviceData = [];
          let data = JSON.parse(body);
          if (!error && data.elements) {
            data.elements.forEach(employee => {
              let obj = {};
              if (employee.nickname) {
                obj.title = `${employee.name} (${employee.nickname})`;
              } else {
                obj.title = `${employee.name}`;
              }
              obj.id = employee.id;
              deviceData.push(obj);
            });
          }
          callback(null, {
            _m : "API returned offers data successfully",
            _tk : query,
            _d : {
              _o : offers,
              _l : franchise.address,
              _d : deviceData
            }
          });
        });
      } else if (franchise.pos.type == 'square' && franchise.pos.access) {
        let options = {
         url: `${process.env.SQUARE_API}/v2/locations`,
         method: 'GET',
         headers: {
           'Authorization': 'Bearer ' + cryptr.decrypt(franchise.pos.access),
            'Content-Type': 'application/json'
         }
        };
        return request.get(options, function(error, res, body) {
          let deviceData = [];
          let data = JSON.parse(body);
          if (!error && data.locations) {
            data.locations.forEach(location => {
              let obj = {};
              obj.title = location.name;
              obj.id = location.id;
              deviceData.push(obj);
            });
          }

          callback(null, {
            _m : "API returned offers data successfully",
            _tk : query,
            _d : {
              _o : offers,
              _l : franchise.address,
              _d : deviceData
            }
          });
        });
      }

      callback(null, {
        _m : "API returned offers data successfully",
        _tk : query,
        _d : {
          _o : offers,
          _l : franchise.address
        }
      });
    }).catch((error) => {
      console.log(error);
      return callback({"code" : 500}, null);
    });
  });
}

function EditorData(auth, id, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    try {
      mongoose.Types.ObjectId(id);
    } catch(Error) {
      return callback(null, {
        _m : "Offer was not found",
        _tk : auth,
        _d : {
          _o : null,
          _l : null
        }
      });
    }

    Offers.one({_id : id, franchise : franchise._id, suspended : false, deleted : false}, 'r').then((offer) => {

      if (!offer) {
        return callback(null, {
          _m : "Offer was not found",
          _tk : auth,
          _d : {
            _o : null,
            _l : null
          }
        });
      }

      let locations = [];
      let locid = [];

      for (let addr of offer.locations) {
        locid.push(addr.formatted.toString());
      }

      for (let location of franchise.address) {
        let obj = {
          "i" : location._id,
          "f" : location.formatted,
          "c" : locid.includes(location.formatted.toString())
        };
        locations.push(obj);
      }

      callback(null, {
        _m : "API returned offer data successfully",
        _tk : auth,
        _d : {
          _o : offer,
          _l : locations
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UploadOffer(body, file, callback) {
  const auth = body.auth;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Offers.new(franchise, body, file).then((offer) => {
      franchise.offers.push(offer.id);

      franchise.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success",
          _o : offer
        });
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function OfferAnalyticsData(auth, id, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Offers.one({_id : id, franchise : franchise._id, deleted : false}, 'f', true).then((offer) => {

      if (!offer) {
        return callback(null, {
          _m : "offer_not_found",
          _tk : auth,
          _d : {
            _v : false
          }
        });
      }

      let name = offer.name;
      let redeemsAmount = offer.redeemsAmount;
      let refund = offer.refund * redeemsAmount;
      let helpedSaved = offer.savings * redeemsAmount;
      let lifespan = Math.floor((moment().valueOf() - offer.created.getTime()) / 1000 / 60 / 60 / 24);

      let chartData = [
        {data : [], labels : []}
      ];

      Redeems.of({offer : id}, 'f', true).then((redeems) => {
        let a = {};
        for (let redeem of redeems) {
          if (a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`]) {
            a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`].redeems += 1;
            if (a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`][`${redeem.date.getMonth() + 1}/${redeem.date.getDate()}`]) {
              a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`][`${redeem.date.getMonth() + 1}/${redeem.date.getDate()}`] += 1;
            } else {
              a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`][`${redeem.date.getMonth() + 1}/${redeem.date.getDate()}`] = 1;
            }
          } else {
            a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`] = {
              redeems : 1
            };
            a[`${redeem.date.getMonth()+1}/${redeem.date.getFullYear()}`][`${redeem.date.getMonth() + 1}/${redeem.date.getDate()}`] = 1;
          }
        }

        for (let b in a) {
          chartData[0].data.push(a[b].redeems);
          chartData[0].labels.push(b);
          let d = [];
          let l = [];
          for (let c in a[b]) {
              if (c !== "redeems") {
                d.push(a[b][c]);
                l.push(c);
              }
          }
          chartData.push({
            data: d,
            labels: l
          });
        }

        callback(null, {
          _m : "API returned offer data successfully",
          _tk : auth,
          _d : {
            _n : name,
            _r : redeemsAmount,
            _mb : refund,
            _hs : helpedSaved,
            _l : lifespan,
            _c : chartData,
            _v : true
          }
        });
      }).catch((error) => callback({"code" : 500}, null));
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UpdateOffer(body, callback) {
  const auth = body.auth;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Offers.update(body, franchise).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UpdateTab(id, redeem) {
  Franchise.findOne({_id : id, deleted : false}, function(error, franchise) {
    if (error) return;

    if (!franchise) return;

    franchise.redeems.push(redeem._id);
    franchise.tab.amount += redeem.revenue;
    franchise.tab.redeems.push({
      amount: redeem.revenue,
      date: moment().toDate()
    });
    franchise.updated = moment().toDate();

    if (redeem.revenue == 0) {
      franchise.save();
      return;
    }
    stripe.transfers.create({
      amount: redeem.revenue * 100,
      currency: "usd",
      destination: franchise.stripe.customer
    }, function(err, transfer) {
      // asynchronously called
      franchise.save();
    });
  });
}

function PauseOffer(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var fat = jwt.decode(auth);

  Offers.one({_id : id, deleted : false}, 'f', true).then((offer) => {
    if (offer.franchise.crypto !== fat._tk) return callback({"code" : 402}, null);

    if (!offer) {
      return callback(null, {
        _m : "_notfound",
        _tk : auth,
        _d : {
          _p : null
        }
      });
    }

    offer.paused = !offer.paused;
    offer.save(function(error, doc) {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "API returned offer data successfully",
        _tk : auth,
        _d : {
          _p : offer.paused
        }
      });
    });
  }).catch((error) => {
    return callback({"code" : 500}, null);
  });
}

function DeleteOffer(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Offers.remove(body, franchise).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ReviewsData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Reviews.of({business : franchise._id}).then((reviews) => {
      callback(null, {
        _m : "API returned offers data successfully",
        _tk : auth,
        _d : {
          _r : reviews
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ReviewAnalyticsData(auth, id, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    Reviews.one({_id : id, business : franchise._id, deleted : false}, 'f', true).then((review) => {

      if (!review) {
        return callback(null, {
          _m : "offer_not_found",
          _tk : auth,
          _d : {
            _r : false
          }
        });
      }

      let responses = [];

      review.responses.forEach((response) => {
        let resp = {};
        resp.date = response.date;
        if (review.questions.includes('rating')) {
          resp.rating = response.rating;
        }
        if (review.questions.includes('overall_exp')) {
          resp.overall_exp = response.overall_exp;
        }
        if (review.questions.includes('addition')) {
          resp.addition = response.addition;
        }
        if (review.questions.includes('rec')) {
          resp.rec = response.recommend;
          resp.recWhy = response.recExplain;
        }
        responses.push(resp);
      });

      callback(null, {
        _m : "API returned offer data successfully",
        _tk : auth,
        _d : {
          _r : responses
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function EditorReviewsData(auth, id, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    try {
      mongoose.Types.ObjectId(id);
    } catch(Error) {
      return callback(null, {
        _m : "Review was not found",
        _tk : auth,
        _d : {
          _r : null,
          _l : null
        }
      });
    }

    Reviews.one({_id : id, business : franchise._id, deleted : false}, 'r').then((review) => {

      if (!review) {
        return callback(null, {
          _m : "Review was not found",
          _tk : auth,
          _d : {
            _r : null
          }
        });
      }

      callback(null, {
        _m : "API returned offer data successfully",
        _tk : auth,
        _d : {
          _r : review
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UploadReview(body, file, callback) {
  const auth = body.auth;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Reviews.new('business', franchise, body, file).then((review) => {
      franchise.reviews.push(review.id);

      franchise.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success",
          _r : review
        });
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UpdateReview(body, callback) {
  const auth = body.auth;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Reviews.update(body, 'business', franchise._id).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function DeleteReview(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Reviews.remove(body, franchise).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function RedeemsData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}).populate({
    path: 'redeems',
    populate: {
      path: 'redeemer'
    }
  }).exec(function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    redeems = franchise.redeems.reverse();

    let redarr = [];

    redeems.forEach((redeem) => redarr.push({
      id : redeem.offer._id,
      title : redeem.offer.name,
      user : redeem.redeemer ? redeem.redeemer.fullname.substring(0,3) : 'Anonymous',
      revenue : redeem.revenue,
      date : redeem.date
    }));

    callback(null, {
      _m : "API returned settings data successfully",
      _tk : auth,
      _d : {
        _r : redarr
      }
    });

  });
}

function SettingsData(auth, callback) {
  var fat = jwt.decode(auth);
  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 401}, null);

    let locations = [];

    for (let location of franchise.address) {
      let obj = {
        "i" : location._id,
        "f" : location.formatted,
        "d" : false
      };
      locations.push(obj);
    }

    let data = {
      "_a" : {
        "email" : franchise.email
      },
      "_f" : {
        "name" : franchise.name,
        "owner" : franchise.owner,
        "website" : franchise.website || ''
      },
      "_le" : {
        "valid" : franchise.stripe.customer,
        "_stripe_source_tkn": crypto.randomBytes(9).toString("hex"),
        "_login": ""
      },
      "_m" : {
        "mc" : franchise.manage.code,
        "pos": franchise.pos.identifier
      },
      "_l" : locations,
      "_p" : {
        "notif" : franchise.permissions.notifications
      }
    };

    stripe.accounts.createLoginLink(
      franchise.stripe.customer,
      function(err, link) {
        // asynchronously called
        if (link) {
          data._le._login = link.url;
        }
        callback(null, {
          _m : "API returned settings data successfully",
          _tk : auth,
          _d : data
        });
      }
    );
  });
}

function UpdateAccount(body, callback) {
  const auth = body.auth;
  const email = body.email.toLowerCase().trim();
  const password = body.password;
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    Franchise.findOne({crypto : {$ne : fat._tk}, email : email}, function(error, existingEmail) {
      if (error) return callback({"code" : 500}, null);

      if (existingEmail) return callback(null, {_m : "_emailinuse"});

      if (password !== "" && password.length < 4) {
        return callback(null, {_m : "_passshort"});
      }

      franchise.email = email;

      if (password !== "") {
        var hashedPassword = bcrypt.hashSync(password, 11);
        franchise.password = hashedPassword;
      }
      franchise.updated = moment().toDate();

      franchise.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success"
        });
      });
    });
  });
}

function UpdateFranchise(body, callback) {
  const auth = body.auth;
  const name = body.name.trim();
  const owner = body.owner.trim();
  const website = body.website.trim();
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    franchise.name = name;
    franchise.owner = owner;
    franchise.website = website;
    franchise.updated = moment().toDate();

    franchise.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success"
      });
    });
  });
}

function UpdateLocation(body, callback) {
  const auth = body.auth;
  const formatted = body.address;
  const lat = body.lat;
  const lng = body.lng;
  const postal = body.postal;
  const previous = body.previous;
  var kept = [];
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    for (let prev of previous) {
      if (!prev.d) {
        kept.push(prev.i);
      }
    }

    franchise.address = franchise.address.filter(addr => {
      return kept.includes(addr._id.toString());
    });

    if (formatted && lat && lng && postal) {
      let newLocation = {
        formatted : formatted,
        geo : [lng, lat],
        zipcode : postal
      };

      franchise.address.push(newLocation);
    }
    franchise.updated = moment().toDate();
    franchise.save((error, updated) => {
      if (error) return callback({"code" : 500}, null);

      let locations = [];

      for (let location of franchise.address) {
        let obj = {
          "i" : location._id,
          "f" : location.formatted,
          "d" : false
        };
        locations.push(obj);
      }

      callback(null, {
        _m : "_success",
        _data : {
          "l" : locations
        }
      });
    });
  });
}

function VerifyClover(body, callback) {
  const merchantID = body.merchantID;
  const employeeID = body.employeeID;
  const clientID = body.clientID;
  const code = body.code;
  const identifierCode = body.accessCode;

  request.get(`${process.env.CLOVER_ENDPOINT}/oauth/token?client_id=${clientID}&client_secret=${process.env.CLOVER_APP_SECRET}&code=${code}`, {}, function(error, res, body) {
    let data = JSON.parse(body);
    if (error || !data.access_token) {
      console.log(error);
      console.log(data);
      return callback({code: 401}, null);
    }

    One({"pos.identifier": identifierCode}).then(franchise => {
      franchise.pos.type = "clover";
      franchise.pos.access = cryptr.encrypt(data.access_token);
      if (!franchise.pos.data) {
        franchise.pos.data = {};
      }
      franchise.pos.data.merchant_id = merchantID;
      franchise.updated = moment().toDate();
      franchise.save(error => {
        if (error) return callback({"code" : 500}, null);
        return callback(null,{_s: "_cloververified"});
      });
    }).catch((error) => {
      console.log(error);
      callback({"code" : 401}, null);
    });
  });
}

function VerifySquare(body, callback) {
  const code = body.code;
  const identifierCode = body.accessCode;

  request.post(process.env.SQUARE_API+'/oauth2/token', { json: { client_id: process.env.SQUARE_APP_ID, client_secret: process.env.SQUARE_APP_SECRET, code: code, grant_type: 'authorization_code' }}, function (error, res, data) {
    if (error || !data.access_token) {
      console.log(error);
      console.log(data);
      return callback({code: 401}, null);
    }
    One({"pos.identifier": identifierCode}).then(franchise => {
      franchise.pos.type = "square";
      franchise.pos.access = cryptr.encrypt(data.access_token);
      if (!franchise.pos.data) {
        franchise.pos.data = {};
      }
      franchise.pos.data.merchant_id = data.merchant_id;
      franchise.updated = moment().toDate();
      franchise.save(error => {
        if (error) return callback({"code" : 500}, null);
        return callback(null,{_s: "_squareverified"});
      });
    }).catch((error) => {
      console.log(error);
      callback({"code" : 401}, null);
    });
  });
}

function VerifyStripe(body, callback) {
  const state = body.state;
  const code = body.code;
  request.post(
      'https://connect.stripe.com/oauth/token',
      { json: { client_secret: process.env.STRIPE_SECRET, code: code, grant_type: 'authorization_code' } },
      function (error, res, body) {
          if (!error && res.statusCode == 200) {
              stripe.accounts.retrieve(res.body.stripe_user_id).then((account) => {
                One({email: account.email}).then((franchise) => {
                  if (franchise.stripe.customer) {
                    return callback({"code" : 500}, null);
                  }
                  franchise.stripe.customer = account.id;
                  franchise.stripe.access = res.body.access_token;
                  franchise.updated = moment().toDate();
                  franchise.save(error => {
                    if (error) return callback({"code" : 500}, null);
                    stripe.accounts.update(
                      franchise.stripe.customer,
                      {settings: {payouts: {schedule: {interval: 'manual'}}}},
                        function(err, account) {
                        // asynchronously called
                        return callback(null,{_s: "_stripeverified"});
                      }
                    );
                  })
                }).catch((error) => {
                  callback({"code" : 401}, null);
                });
              }, (error) => {
              });
          } else {
            callback({"code" : 500}, null);
          }
      }
  );
}

function UpdateManagers(body, callback) {
  const auth = body.auth;
  const mcode = body.mcode;
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    franchise.manage.code = crypto.randomBytes(5).toString("hex") + "_man_" + crypto.randomBytes(5).toString("hex");
    franchise.updated = moment().toDate();

    const ver = parseInt(Math.random() * 100000);
    const imagepath = path.join(process.cwd(), `tmp/qr_manager_code_${franchise._id}.svg`);
    var fcode_qr = qr.image(`_i:${franchise._id};_m:${franchise.manage.code};_d:${moment().valueOf()};_f:${franchise._id};;`, { type: 'svg', size: 15 });
    fcode_qr.pipe(fs.createWriteStream(imagepath)).on('finish', () => {
      cloudinary.v2.uploader.upload(imagepath, {folder : `${process.env.CLOUDINARY_FOLDER}/businesses/${franchise._id}`, public_id : ver+"_manager"}, function(error, result) {
        if (error) return callback({"code" : 500}, null);
        fs.unlink(imagepath,  (err) => {});

        franchise.save((error) => {
          if (error) return callback({"code" : 500}, null);

          Mailer.sendManagerCode(franchise, result);
          callback(null, {
            _m : "_success",
            _d : {
              _c : franchise.manage.code
            }
          });
        });
      });
    });
  });
}

function UpdatePos(body, callback) {
  const auth = body.auth;
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    franchise.pos.identifier = crypto.randomBytes(5).toString("hex");
    franchise.updated = moment().toDate();
    franchise.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success",
        _d : {
          _c : franchise.pos.identifier
        }
      });
    });
  });
}

function RedeemTab(body, callback) {
  const auth = body.auth;
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    if (franchise.tab.amount < 15) return callback(null, {
        _m : "_tabtoolow"
      });

      stripe.payouts.create({
        amount: franchise.tab.amount * 100,
        currency: 'usd',
        description: "Mercial franchise tab redeem",
        statement_descriptor: "Mercial payout"
      }, {
        stripe_account: franchise.stripe.customer,
      }).then((payout) => {
        // asynchronously called
        franchise.tab.redeems.push({
          amount : franchise.tab.amount,
          date : payout.created
        });
        franchise.tab.amount = 0.00;

        franchise.save((error) => {
          if (error) return callback({"code" : 500}, null);

          callback(null, {
            _m : "_success"
          });
        });
      }, err => {
        return callback(null, {
          _m : "_errorpayout"
        });
      });
  });
}

function UpdatePrivacy(body, callback) {
  const auth = body.auth;
  const notif = body.notif;
  var fat = jwt.decode(auth);

  Franchise.findOne({crypto : fat._tk, deleted : false}, function(error, franchise) {
    if (error) return callback({"code" : 500}, null);

    if (!franchise) return callback({"code" : 402}, null);

    franchise.permissions.notifications = notif;

    franchise.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success"
      });
    });
  });
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

const Redeem = require('../models/redeem.js');
const mongoose = require('mongoose');
const GOOGLE_API_KEY = process.env.GOOGLE_API;
const googleMapsClient = require('@google/maps');
const moment = require('moment');

var exports = {};
exports.one = One;
exports.new = New;
exports.of = Of;
module.exports = exports;

function New(data, offer, user) {
  return new Promise((resolve, reject) => {

    let zipcode = null;
    let revenue = offer.refund;

    if (offer.locations.length == 1) {

      const mapsAPI = googleMapsClient.createClient({
        key: process.env.GOOGLE_SERVER_API,
        Promise: Promise
      });

      mapsAPI.reverseGeocode({latlng: `${offer.locations[0].geo[0]},${offer.locations[0].geo[1]}`}).asPromise().then((response) => {
        let res = response.json.results[0];
        let zipcode = ["", ""];

        res.address_components.forEach((component) => {
          if (component.types.includes("postal_code")) {
            zipcode[0] = component.long_name;
          }

          if (component.types.includes("postal_code_suffix")) {
            zipcode[1] = component.long_name;
          }
        });

        var newRedeem = new Redeem({
          franchise : data._f,
          offer : data._o,
          redeemer : user,
          zipcode : zipcode[1] !== "" ? zipcode.join("-") : zipcode[0],
          saved : offer.savings,
          revenue : revenue,
          date : moment().toDate()
        });

        newRedeem.save((error, savedRedeem) => {
          if (error) return reject(error);

          resolve(savedRedeem);
        });
      }).catch((error) => reject(error));
    } else {

      var newRedeem = new Redeem({
        franchise : data._f,
        offer : data._o,
        redeemer : user,
        zipcode : zipcode,
        saved : parseFloat(offer.savings),
        revenue : revenue,
        date : moment().toDate()
      });

      newRedeem.save((error, savedRedeem) => {
        if (error) return reject(error);

        resolve(savedRedeem);
      });
    }
  });
}

function One(query, service = 'f', raw = false) {
  return new Promise((resolve, reject) => {
    Redeem.findOne(query).populate('franchise').populate('offer').populate("redeemer").exec(function(error, redeem) {
      if (error) return reject(error);

      if (!redeem) {
        return resolve(false);
      }

      if (raw) {
        return resolve(redeem);
      }

      resolve(FormulateJSON([redeem], service, true));
    });
  });
}

function Of(query, service = 'f', raw = false) {
  return new Promise((resolve, reject) => {
    Redeem.find(query).sort({created : 1}).populate('franchise').populate('offer').populate("redeemer").exec(function(error, redeems) {
      if (error) return reject(error);

      if (raw) {
        return resolve(redeems);
      }

      resolve(FormulateJSON(redeems, service));
    });
  });
}

function FormulateJSON(arr, service, one = false) {
  let json = [];

  if (service === 'f') {
    for (let offer of arr) {
      let element = {};
      let locations = [];

      for (let location of offer.locations) {
        locations.push({
          formatted : location.formatted,
          lat : location.geo[1],
          lng : location.geo[0]
        });
      }
      element.id = offer._id;
      element.franchise = {};
      element.franchise.name = offer.franchise.name;
      element.name = offer.name;
      element.description = offer.description;
      element.image = offer.image;
      element.redeem_code = offer.redeem_code;
      element.locations = locations;
      element.website = offer.website || null;
      element.merchips = offer.merchips;
      element.savings = `$${offer.savings.toFixed(2)}`;
      json.push(element);
    }
  } else {
    json = arr;
  }

  if (one) {
    return json[0];
  }

  return json;
}

const User = require('../models/user.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const Mercials = require("./mercials.js");
const Analyzer = require("./analyzer.js");
const Franchises = require("./franchises.js");
const Advertisers = require("./advertisers.js");
const FAQS = require("./faqs.js");
const Offers = require("./offers.js");
const Reviews = require("./reviews.js");
const Redeems = require("./redeems.js");
const Codes = require("./codes.js");
const Mailer = require("./mailer.js");
const GOOGLE_API_KEY = process.env.GOOGLE_SERVER_API;
var stripe = require("stripe")(process.env.STRIPE_SECRET);
const googleMapsClient = require('@google/maps');
const mongoose = require("mongoose");
const io = require("../routes/io");
const moment = require('moment');
const scheduler = require("./scheduler");
const request = require('request');
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.POS_ACCESS_SECRET);

var exports = {};
exports.createUser = CreateUser;
exports.loginUser = LoginUser;
exports.forgot = ForgotPassword;
exports.reset = ResetPassword;
exports.deleteAccount = DeleteAccount;
exports.analyticsData = AnalyticsData;
exports.report = Report;
exports.rate = Rate;
exports.purchase = Purchase;
exports.zes = ZipcodeEnhancerSystem;
exports.feedData = FeedData;
exports.feedItem = FeedItem;
exports.addInterest = AddInterest;
exports.watchedMercial = WatchedMercial;
exports.helpData = HelpData;
exports.reviewsData = ReviewsData;
exports.reviewAnswered = ReviewAnswered;
exports.offersData = OffersData;
exports.offersItem = OffersItem;
exports.fastQR = FastQR;
exports.settingsData = SettingsData;
exports.createReferral = CreateReferral;
exports.updateAccount = UpdateAccount;
exports.updateTarget = UpdateTarget;
exports.updateLocation = UpdateLocation;
exports.updatePreferences = UpdatePreferences;
exports.updatePrivacy = UpdatePrivacy;
exports.validateRedeem = ValidateRedeem;

exports.all = All;
exports.one = One;
module.exports = exports;

function All(query = {}, limit = 30, skip = 0) {
  return new Promise((resolve, reject) => {
    User.find(query).sort({created : -1}).limit(limit).skip(skip).populate('views.mercial').exec().then((error, users) => {
      if (error) reject(error);

      resolve(users);
    });
  });
}

function One(query) {
  return new Promise((resolve, reject) => {
    User.findOne(query).populate('views.mercial').exec(function(error, user) {
      if (error) reject(error);

      resolve(user);
    });
  });
}

function CreateUser(body, callback) {
  const fullname = body.fullname;
  const email = body.email.toLowerCase().trim();
  const password = body.password;
  const birthdate = body.birthdate.replace(/\//g, "-");
  const referral = body.referral ? body.referral : null;

  if (!birthdateIsValid(birthdate, 13)) {
    return callback(null, {_m : "_dobinvalid", _tk : null});
  }

  if (password.length < 4) {
    return callback(null, {_m : "_passshort", _tk : null});
  }

  User.findOne({email : email}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (user) return callback(null, {_m : "_emailinuse", _tk : null});

    var hashedPassword = bcrypt.hashSync(password, 11);

    const key = crypto.randomBytes(8).toString("hex") + "." + crypto.randomBytes(4).toString("hex") + "." + crypto.randomBytes(6).toString("hex");
    const socket = crypto.randomBytes(12).toString("hex") + "." + crypto.randomBytes(32).toString("hex") + "." + crypto.randomBytes(16).toString("hex");

    var newUser = new User({
      crypto : key,
      socket : socket,
      fullname : fullname,
      email : email,
      password : hashedPassword,
      redeem_validator : generateRedeemToken(),
      birthdate:  moment(birthdate).toDate(),
      created : moment().toDate(),
    });

    newUser.save(function(error, doc) {
      if (error) {
        return callback({"code" : 500}, null);
      }

      Mailer.welcome(doc, "user").then(() => {
        var token = jwt.sign({_tk : key, _fn : fullname, _s : socket}, process.env.JWT_SECRET, {expiresIn : 86400 * 365});

        callback(null, {
          _m : "Successfully created account",
          _tk : token,
          _fn : fullname,
          _c : 0
        });
        if (referral && referral.length > 3) {
          AddReferralPoints(referral, newUser);
        }
      }).catch((error) => callback({"code" : 500}, null));
    });
  });
}

function AddReferralPoints(code, newUser) {
  User.findOne({referral : code}, function(error, user) {
    if (error || !user) return;

    user.awardedMerchips += 75;
    user.merchips += 75;
    user.updated = moment().toDate();
    user.save();
    newUser.merchips += 75;
    newUser.awardedMerchips += 75;
    newUser.referrer = user._id;
    newUser.save();
  });
}

function LoginUser(body, callback) {
  const email = body.email.toLowerCase().trim();
  const password = body.password;

  User.findOne({email : email}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback(null, {_m : "_emailinvalid", _tk : null});

    if (user.locked) {
      return callback(null, {_m : "_accountlocked", _tk : null});
    }

    if (!bcrypt.compareSync(password, user.password)) {
      user.failed = user.failed + 1;
      if (user.failed >= 5) {
        user.locked = true;
      }
      user.save();
      return callback(null, {_m : "_passinvalid", _tk : null});
    }

    user.failed = 0;
    user.deleted = false;

    user.save();

    var token = jwt.sign({_tk : user.crypto, _fn : user.fullname, _s : user.socket}, process.env.JWT_SECRET, {expiresIn : 86400 * 365});

    callback(null, {
      _m : "Successfully logged in",
      _tk : token,
      _fn : user.fullname,
      _c : user.merchips
    });
  });
}

function ForgotPassword(body, callback) {
  const email = body.email.toLowerCase().trim();
  const birthdate = moment(body.birthdate.replace(/\//g, "-")).toDate();

  User.findOne({email : email, birthdate: birthdate, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback(null, {_m : "_mismatch", _tk : null});

    let length = Math.floor(Math.random() * (4 - 3 + 1)) + 3;
    const code = crypto.randomBytes(length).toString("hex");
    Codes.new({owner : user._id, type : 'reset', code : code, days : 1}).then(() => Mailer.resetCode(user, code)).then((res) => {
      callback(null, {
        _m : "Successfully sent code"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ResetPassword(body, callback) {
  const email = body.email.toLowerCase().trim();
  const code = body.code;
  const password = body["new-password"];

  if (password.length < 4) {
    return callback(null, {_m : "_passshort", _tk : null});
  }

  User.findOne({email : email, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback(null, {_m : "_mismatch", _tk : null});

    Codes.verify({owner : user._id, type : 'reset', code : code}).then((code) => {
      if (code) {
        var hashedPassword = bcrypt.hashSync(password, 11);
        user.password = hashedPassword;
        user.save(function(error) {
          Codes.invalidate(code).then(()=>{
            callback(null, {
              _m : "_success"
            });
          });
        });
      } else {
        callback(null, {
          _m : "_invalidcode"
        });
      }
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function DeleteAccount(body, callback) {
  var uat = jwt.decode(body.auth);
  User.findOne({crypto : uat._tk, deleted : false}).exec(function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    user.deleted = true;
    user.updated = moment().toDate();

    user.save(function(error) {
      if (error) return callback({"code" : 500}, null);

      scheduler.deleteAccount(user, "client");

      callback(null, {
        _m : "API deleted user account successfully",
        _tk : body.auth,
        _d : null
      });
    });
  });
}

function AnalyticsData(auth, callback) {
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}).populate('views.mercial').populate("revsAnswered").populate("redeemed").exec(function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Analyzer.userDashboard(user).then((data) => {
      callback(null, {
        _m : "API returned settings data successfully",
        _tk : auth,
        _d : data
      });
    }).catch((error) => {
      console.log(error);
      callback({"code" : 500}, null);
    });
  });
}

function Report(body, callback) {
  const auth = body.auth;
  const id = body.id;
  const type = body.type;
  const reason = body.reason;
  const expand = body.expand;

  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    if (body.type == "mercial") {
      Mercials.addReport(body, user._id).then((mercial) => {

        if (!mercial) {
          return callback(null, {
            _m : "_invalidid",
            _tk : auth
          });
        }

        callback(null, {
          _m : "success",
          _tk : auth
        });
      }).catch((error) => {
        return callback({"code" : 500}, null);
      });
    } else {
      Offers.addReport(body, user._id).then((offer) => {

        if (!offer) {
          return callback(null, {
            _m : "_invalidid",
            _tk : auth
          });
        }

        callback(null, {
          _m : "success",
          _tk : auth
        });
      }).catch((error) => {
        return callback({"code" : 500}, null);
      });
    }
  });
}

function Purchase(body, callback) {
  const auth = body.auth;
  const checkoutID = body.data.checkoutID;
  // const price = body.data.price;
  // const token = body.data.token;
  // const productID = body.data.productID;

  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    if (user.transactions.find(entry => entry.checkoutSession == checkoutID)) {
      return callback(null, {
        _m : "exists",
        _tk : auth
      });
    }

    stripe.checkout.sessions.retrieve(checkoutID, (error, session) => {
      if (error) {
        return callback(null, {
          _m : "error",
          _tk : auth
        });
      }
      amount = 0;
      price = 0;
      products = [];
      for (let item of session.display_items) {
        price += item.amount * item.quantity;
        if (item.amount == "150") {
          amount += 1000;
          products.push("MER_1K");
        } else if (item.amount == "375") {
          amount += 2500;
          products.push("MER_2.5K");
        } else if (item.amount == "750") {
          amount += 5000;
          products.push("MER_5K");
        } else if (item.amount == "1500") {
          amount += 10000;
          products.push("MER_10K");
        }
      }
      user.transactions.push({
        checkoutSession: checkoutID,
        customerID: session.customer,
        price: price,
        products: products,
        date: moment().toDate()
      });
      user.awardedMerchips += amount;
      user.merchips += amount;
      user.save();
      callback(null, {
        _m : "success",
        _tk : auth,
        _d: {
          _am: user.merchips
        }
      });
    });
  });
}


function Rate(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var uat = jwt.decode(auth);

  if (!["1","2","3","4","5","6","7","8","9","10"].includes(body.rating.toString())) {
    return callback(null, {
      _m : "_invalidrating",
      _tk : auth,
      _d : {
        r : null
      }
    });
  }

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Offers.addRating(body).then((rating) => {
      callback(null, {
        _m : "success",
        _tk : auth,
        _d : {
          r : rating
        }
      });
    }).catch((error) => callback({"code" : 500}, null));
  });
}

function ZipcodeEnhancerSystem(body, callback) {
  const auth = body.auth;
  const id = body.id;
  const rid = body.rid;
  const location = body.location;

  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Redeems.one({_id : rid, offer : id, redeemer : user._id}).then((redeem) => {
      if (!redeem) {
        return callback(null, {
          _m : "_invalidlocation",
          _tk : auth,
          _d : {
            z : null
          }
        });
      }

      const mapsAPI = googleMapsClient.createClient({
        key: process.env.GOOGLE_SERVER_API,
        Promise: Promise
      });

      mapsAPI.geocode({latLng: `${location.lat},${location.lng}`}).asPromise().then((response) => {
        let res = response.json.results[0];

        redeem.zipcode = res.address_components[res.address_components.length - 1].long_name;
      }).catch(error => callback({"code" : 500}, null));
    }).catch(error => callback({"code" : 500}, null));
  });
}

function FeedData(data, callback) {
  var uat = jwt.decode(data.auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    let age = moment().diff(user.birthdate, 'years', false);

    let tagsFilter = user.preferences.tags;

    let ethnicityFilter = ["any"];
    let languagesFilter = ["any"];
    let locationTypeFilter = ["none"];
    let locationFilter = ["any"];
    let lang = "en";
    if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
      lang = data.lang;
    }

    languagesFilter.push(lang);

    if (user.target.ethnicity != "") {
      ethnicityFilter.push(user.target.ethnicity);
    }

    if (user.target.languages.length > 0) {
      ethnicityFilter.push(...user.target.languages);
    }

    if (user.target.country != "") {
      locationTypeFilter.push("country");
      locationFilter.push(user.target.country);
    }

    if (user.target.state != "") {
      locationTypeFilter.push("state");
      locationFilter.push(user.target.state);
    }

    if (user.target.zipcode != "") {
      locationTypeFilter.push("zipcode");
      locationFilter.push(user.target.zipcode.formatted);
    }

    let sortFilter = {};

    if (data.meta == "views") {
      sortFilter.views = -1;
      tagsFilter = [];
    } else if (data.meta == "merchips") {
      sortFilter.merchips = -1;
      tagsFilter = [];
    } else if (data.meta == "duration") {
      sortFilter["metadata.duration"] = -1;
      tagsFilter = [];
    }
    sortFilter.created = data.date == "newest" ? -1 : 1;

    if (data.categories && data.categories != "null") {
      data.categories = data.categories.split(",");
      tagsFilter = data.categories.map((value) => value.toUpperCase());
    }

    Mercials.all({deleted : false, suspended : false, paused : false, forcedPause : false, "target.ethnicities" : { $elemMatch : { $in : ethnicityFilter}}, "target.languages" : { $elemMatch : { $in : languagesFilter}}, "target.locationType" : { $elemMatch : { $in : locationTypeFilter}}, "target.locations" : { $elemMatch : { $in : locationFilter}}, "age.lower" : {$lte : age}, "age.upper" : {$gte : age}, tags : { $elemMatch : { $in : tagsFilter }}}, 'f', 5, 5 * data.prevPop, sortFilter, false, lang).then((mercials) => {
      if (mercials.length < 5) {
        let needed = 5 - mercials.length;
        let unwantedIDs = [];
        mercials.forEach((mercial)=> {
          unwantedIDs.push(mercial.id)
        });

        Mercials.all({ _id : {$nin : unwantedIDs}, deleted : false, suspended : false, paused : false, forcedPause : false, "target.ethnicities" : { $elemMatch : { $in : ethnicityFilter}}, "target.languages" : { $elemMatch : { $in : languagesFilter}}, "target.locationType" : { $elemMatch : { $in : locationTypeFilter}}, "target.locations" : { $elemMatch : { $in : locationFilter}}, "age.lower" : {$lte : age}, "age.upper" : {$gte : age}, tags : { $elemMatch : { $nin : tagsFilter}}}, 'f', needed, 5 * data.prevPop, sortFilter, false, lang).then((extra) => {

          callback(null, {
            _m : "API returned feed data successfully",
            _tk : data.auth,
            _d : {
              _f : [...mercials,...extra]
            }
          });
        }).catch((error) => {
          callback({"code" : 500}, null)
        });
      } else {
        callback(null, {
          _m : "API returned feed data successfully",
          _tk : data.auth,
          _d : {
            _f : mercials
          }
        });
      }
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function FeedItem(data, id, callback) {
  const auth = data.auth;
  var uat = jwt.decode(auth);
  let lang = "en";
  if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
    lang = data.lang;
  }
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    let age = (moment().toDate() - moment(user.birthdate).toDate()) / (8.64e+7 * 365);

    try {
      mongoose.Types.ObjectId(id);
    } catch(Error) {
      return callback(null, {
        _m : "API returned item data successfully",
        _tk : data.auth,
        _d : {
          _i : null
        }
      });
    }

    Mercials.one({_id : id, deleted : false, suspended : false, paused : false, forcedPause : false, "age.lower" : {$lte : age}, "age.upper" : {$gte : age}}, 'u', false, lang).then((mercial) => {
      callback(null, {
        _m : "API returned item data successfully",
        _tk : data.auth,
        _d : {
          _i : mercial
        }
      });
    }).catch((error) => callback({"code" : 500}, null));
  });
}

function AddInterest(body, callback) {
  const auth = body.auth;
  const id = body.id;
  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Mercials.one({_id : id, deleted : false}, 'f', true).then((mercial) => {

      if (!mercial) {
        return callback(null, {
          _m : "_invalid",
          _tk : auth
        });
      }

      mercial.metadata.interest = mercial.metadata.interest + 1;
      mercial.save();

      callback(null, {
        _m : "success",
        _tk : auth
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function WatchedMercial(body, callback) {
  const auth = body.auth;
  const id = body.id;
  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}).exec(function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Mercials.addView(user, {_id : id, deleted : false}).then((mercial) => {
      if (mercial) {
        Advertisers.addToTab({_id : mercial.advertiser._id}, {size : mercial.size, seconds : mercial.metadata.duration}).catch(() => {
          return callback(null, {
            _m : "_unavailable",
            _tk : auth,
            _d : {
              _mr : 0,
              _ma : user.merchips
            }
          });
        });
        user.views.push({
          mercial : mercial._id,
          date : moment().toDate(),
          merchips: mercial.merchips
        });
        user.updated = moment().toDate();
        user.merchips += mercial.merchips;
        user.save();
        callback(null, {
          _m : "success",
          _tk : auth,
          _d : {
            _mr : mercial.merchips,
            _ma : user.merchips
          }
        });
      } else {
        callback(null, {
          _m : "_unavailable",
          _tk : auth,
          _d : {
            _mr : 0,
            _ma : user.merchips
          }
        });
      }
    }).catch((error) => {
      if (error === "_mta") {
        return callback(null, {
          _m : "_mta",
          _tk : auth,
          _d : {
            _mr : 0,
            _ma : user.merchips
          }
        });
      }
      return callback({"code" : 500}, null);
    });
  });
}

function HelpData(types, callback) {
  FAQS.all('g', types).then((faqs) => {
    callback(null, {
      _m : "API returned help data successfully",
      _d : {
        _f : faqs
      }
    });
  }).catch((error) => {
    return callback({"code" : 500}, null);
  });
}

function OffersData(data, callback) {
  var uat = jwt.decode(data.auth);
  let lang = "en";
  if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
    lang = data.lang;
  }
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    let sortFilter = {};
    if (data.meta == "redeems") {
      sortFilter.redeemsAmount = -1;
    } else if (data.meta == "highest") {
      sortFilter.merchips = -1;
    } else if (data.meta == "lowest") {
      sortFilter.merchips = 1;
    }
    sortFilter.created = data.date == "newest" ? -1 : 1;

    let miles = 5;
    if (data.location == "two") {
      miles = 2;
    } else if (data.location == "ten") {
      miles = 10;
    } else if (data.location == "fifteen") {
      miles = 15;
    } else if (data.location == "twenty") {
      miles = 20;
    }

    let query = {deleted : false, suspended : false, paused: false};

    if (user.target.zipcode.geo && user.target.zipcode.geo.length > 0) {
      let nearQuery = {
        $geoWithin : {
          $centerSphere : [ user.target.zipcode.geo , miles / 3963.2 ]
        }
      };
      query["locations.geo"] = nearQuery;
    }

    Offers.all(query, 'f', 20, 20 * data.prevPop, sortFilter, lang).then((offers) => {
      for (let offer of offers){
        offer.redeem_verify_code = user.redeem_validator;
        offer.redeemable = user.merchips - offer.merchips >= 0;
      }

      callback(null, {
        _m : "API returned offers data successfully",
        _tk : data.auth,
        _d : {
          _o : offers,
          _z : user.target.zipcode.formatted ? {
            formatted : user.target.zipcode.formatted,
            lat : user.target.zipcode.geo[1],
            lng : user.target.zipcode.geo[0]
          } : null
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function OffersItem(auth, id, callback) {
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    try {
      mongoose.Types.ObjectId(id);
    } catch(Error) {
      return callback(null, {
        _m : "API returned item data successfully",
        _tk : auth,
        _d : {
          _i : null,
          _z : user.target.zipcode.formatted ? {
            formatted : user.target.zipcode.formatted,
            lat : user.target.zipcode.geo[1],
            lng : user.target.zipcode.geo[0]
          } : null
        }
      });
    }

    Offers.one({_id : id, suspended : false, paused: false, deleted : false}).then((offer) => {

      if (!offer) {
        offer = null;
      } else {
        offer.redeem_verify_code = user.redeem_validator;
        offer.redeemable = user.merchips - offer.merchips >= 0;
      }

      callback(null, {
        _m : "API returned item data successfully",
        _tk : auth,
        _d : {
          _i : offer,
          _z : user.target.zipcode.formatted ? {
            formatted : user.target.zipcode.formatted,
            lat : user.target.zipcode.geo[1],
            lng : user.target.zipcode.geo[0]
          } : null
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ReviewsData(data, callback) {
  var uat = jwt.decode(data.auth);
  let lang = "en";
  if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
    lang = data.lang;
  }
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Reviews.all({_id: {$nin: user.revsAnswered}, deleted: false}, 'f', 5, 5 * data.prevPop, lang).then((reviews) => {
      callback(null, {
        _m : "API returned reviews data successfully",
        _tk : data.auth,
        _d : {
          _r : reviews
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function ReviewAnswered(body, callback) {
  const auth = body.auth;
  const id = body.id;
  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}).exec(function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    if (user.revsAnswered.includes(id)) {
      return callback(null, {
        _m : "_aa",
        _tk : auth,
        _d : {
          _mr : 0,
          _ma : user.merchips
        }
      });
    }

    Reviews.answered(id, user, body).then((review) => {
      user.revsAnswered.push(review._id);
      user.save();
      callback(null, {
        _m : "success",
        _tk : auth,
        _d : {
          _mr : review.merchips,
          _ma : user.merchips
        }
      });
    }).catch((error) => {
      if (error === "_mta") {
        return callback(null, {
          _m : "_mta",
          _tk : auth,
          _d : {
            _mr : 0,
            _ma : user.merchips
          }
        });
      }
      return callback({"code" : 500}, null);
    });
  });
}

function FastQR(auth, id, callback) {
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    Offers.one({_id : id}, 'u', true).then(offer => Offers.formulateFastRedeemCode(offer.franchise._id, id, user.redeem_validator)).then(path => {
      return callback(null, path);
    }).catch(error => {
      return callback({"code" : 500}, null);
    });
  });
}

function ValidateRedeem(body, callback) {
  const auth = body.auth;
  const codeData = body.codeData;
  var uat = jwt.decode(auth);

  let seconds = (moment().valueOf() - codeData._d) / 1000;

  if (seconds > 10) {
    io.respond(codeData._channel, false, "prompt_timeout");
    return callback(null, {
      _m : "_failure",
      _tk : auth,
      _d : {
        _s : false
      }
    });
  }

  io.respond(codeData._u._s, true, "_handled", {_sc : codeData._u._sc});

  if (!codeData._u._d) {
    io.respond(codeData._channel, false, "_userdidnotaccept");
    return callback(null, {
      _m : "_failure",
      _tk : auth,
      _d : {
        _s : false
      }
    });
  }

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    let offer;

    Offers.one({deleted : false, suspended : false, franchise : codeData._f, _id : codeData._o}, 'f', true).then((off) => {
      if (!off) {
        io.respond(codeData._channel, false, "_invalidoffer");
        return callback(null, {
          _m : "_invalidoffer",
          _tk : auth,
          _d : {
            _s : false
          }
        });
      }

      offer = off;

      if (user.merchips < offer.merchips) {
        io.respond(codeData._channel, false, "_notenoughmerchips");
        callback(null, {
          _m : "_notenoughmerchips",
          _tk : auth,
          _d : {
            _s : false
          }
        });
        return;
      }

      Redeems.new(codeData, offer, user._id).then((redeem) => {
        user.merchips = user.merchips - offer.merchips;
        user.updated = moment().toDate();
        user.redeemed.push(redeem._id);
        user.save();

        let updatedBarcode = null;
        if (offer.upc) {
          let updatedBarcode = offer.upc + offer.redeemsAmount;
        }
        io.respond(`${user.socket}-${codeData._o}.redeemed`, true, "_redeemedsuccessfully", {_z : offer.locations.length > 1, _m : user.merchips});
        io.respond(codeData._channel, true, "_success", null, {_u: user.fullname.substring(0,3), _d: moment().format(), _s: offer.savings, _r: redeem.revenue, _d: offer.description, _upc: updatedBarcode});

        Franchises.updateTab(off.franchise.id, redeem);

        offer.redeemsAmount += 1;
        offer.updated = moment().toDate();
        offer.save();

        if (codeData._m && codeData._m._d && offer.franchise.pos.type === 'clover') {
          let options = {
            method: 'GET',
            url: `${process.env.CLOVER_API_ENDPOINT}/v3/merchants/${offer.franchise.pos.data.merchant_id}/orders`,
            qs: {
              filter: `employee.id=${codeData._m._d}`,
              access_token: cryptr.decrypt(offer.franchise.pos.access)
            },
            headers: {accept: 'application/json'}
          };
          request.get(options, function(error, res, body) {
            let data = JSON.parse(body);
            try {
              let orderID = data.elements[0].id;
              let options = {
                method: 'POST',
                url: `${process.env.CLOVER_API_ENDPOINT}/v3/merchants/${offer.franchise.pos.data.merchant_id}/orders/${orderID}/discounts`,
                qs: {
                  access_token: cryptr.decrypt(offer.franchise.pos.access)
                },
                headers: {'content-type': 'application/json'},
                body: `{"name":"Mercial Offer","amount":-${offer.savings * 100}}`
              };

              request(options, function (error, response, body) {
                if (error) throw new Error(error);
              });
            } catch(err) {
              console.log(err);
            }
          });
        }

        if (codeData._m && codeData._m._d && offer.franchise.pos.type === 'square') {
          var options = {
            method: 'POST',
            url: `${process.env.SQUARE_API}/v2/orders/search`,
            headers: {
              Authorization: `Bearer ${cryptr.decrypt(offer.franchise.pos.access)}`,
              'Content-Type': 'application/json'
            },
            body: {
              location_ids: [ codeData._m._d ],
              return_entries: true,
              query: {
                sort: { sort_field: 'UPDATED_AT' },
                filter: { states_filter: 'OPEN' }
              }
            },
            json: true
          };

          request(options, function (error, response, body) {
            if (error) throw new Error(error);
            let orderID = body.order_entries[0].order_id;
            var options = {
              method: 'PUT',
              url: `${process.env.SQUARE_API}/v2/locations/${codeData._m._d}/orders/${orderID}`,
              headers: {
                Authorization: `Bearer ${cryptr.decrypt(offer.franchise.pos.access)}`,
                'Content-Type': 'application/json'
              },
              body: {
                idempotency_key: 'MER_'+crypto.randomBytes(8).toString("hex"),
                 order: {
                   version: body.order_entries[0].version,
                    discounts: [ { amount_money: { amount: (offer.savings * 100), currency: 'USD' },
                         name: 'MERCIAL OFFER',
                         scope: 'ORDER',
                         type: 'FIXED_AMOUNT' } ] } },
              json: true };

            request(options, function (error, response, body) {
              if (error) throw new Error(error);
            });
          });
        }

        return callback(null, {
          _m : "_success",
          _tk : auth,
          _d : {
            _s : true,
            _m : user.merchips
          }
        });
      }).catch((error) => {
        console.log(error);
        callback({"code" : 500}, null);
      });
    }).catch((error) => {
      console.log(error);
      callback({"code" : 500}, null)
    });
  });
}

function SettingsData(auth, callback) {
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 401}, null);

    let data = {
      "_a" : {
        "email" : user.email,
        "referral": user.referral
      },
      "_l" : {
        "zipcode" : user.target.zipcode.formatted
      },
      "_p" : {
        "tags" : user.preferences.tags
      },
      "_py" : {
        "notif" : user.permissions.notifications
      },
      "_t" : {
        "eth" : user.target.ethnicity || '',
        "langs" : user.target.languages
      }
    };

    callback(null, {
      _m : "API returned settings data successfully",
      _tk : auth,
      _d : data
    });
  });
}

function CreateReferral(body, callback) {
  const auth = body.auth;
  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    if (!user.referral) {
      const length = Math.ceil((Math.random() * 2) + 2);
      user.referral = crypto.randomBytes(1).toString('hex').toUpperCase() + '$' + crypto.randomBytes(length).toString('hex').toUpperCase();
    }
    user.updated = moment().toDate();
    user.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success",
        _c : user.referral
      });
    });
  });
}

function UpdateAccount(body, callback) {
  const auth = body.auth;
  const email = body.email.toLowerCase().trim();
  const password = body.password;
  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    User.findOne({crypto : {$ne : uat._tk}, email : email}, function(error, existingEmail) {
      if (error) return callback({"code" : 500}, null);

      if (existingEmail) return callback(null, {_m : "_emailinuse"});

      if (password !== "" && password.length < 4) {
        return callback(null, {_m : "_passshort"});
      }

      user.email = email;

      if (password !== "") {
        var hashedPassword = bcrypt.hashSync(password, 11);
        user.password = hashedPassword;
      }

      user.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success"
        });
      });
    });
  });
}

function UpdateTarget(body, callback) {
  const supportedLanguages = ["en", "es"];
  const auth = body.auth;
  const ethnicity = body.ethnicity || '';
  const languages = body.languages.filter((lang) => supportedLanguages.includes(lang)) || '';

  var uat = jwt.decode(auth);

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    if (user.target.ethnicity == "" || !user.target.ethnicity) {
      user.target.ethnicity = ethnicity;
    }
    user.target.languages = languages;

    user.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success"
      });
    });
  });
}

function UpdateLocation(body, callback) {
  const auth = body.auth;
  const zipcode = body.zipcode;
  var uat = jwt.decode(auth);

  const mapsAPI = googleMapsClient.createClient({
    key: process.env.GOOGLE_SERVER_API,
    Promise: Promise
  });

  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    if (zipcode == "") {
      user.target.zipcode.formatted = null;
      user.target.zipcode.geo = null;
      return user.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success"
        });
      });
    }

    mapsAPI.geocode({address: zipcode}).asPromise().then((response) => {
      let res = response.json.results[0];
      let zipcode = ["", ""];

      res.address_components.forEach((component) => {
        if (component.types.includes("postal_code")) {
          zipcode[0] = component.long_name;
        }

        if (component.types.includes("postal_code_suffix")) {
          zipcode[1] = component.long_name;
        }

        if (component.types.includes("locality")) {
          user.target.city = component.long_name;
        }

        if (component.types.includes("administrative_area_level_1")) {
          user.target.state = component.short_name;
        }

        if (component.types.includes("country")) {
          user.target.country = component.short_name;
        }
      });

      user.target.zipcode.formatted = zipcode[1] !== "" ? zipcode.join("-") : zipcode[0];
      user.target.zipcode.geo = [res.geometry.location.lng, res.geometry.location.lat];
      user.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success"
        });
      });
    }).catch((err) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UpdatePreferences(body, callback) {
  const auth = body.auth;
  const tags = body.tags.split(/\s*,\s*/).map((val) => val.trim().toUpperCase());
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    user.preferences.tags = tags;

    user.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success",
        _f : tags
      });
    });
  });
}

function UpdatePrivacy(body, callback) {
  const auth = body.auth;
  const aezp = body.aezp;
  const notif = body.notif;
  var uat = jwt.decode(auth);
  User.findOne({crypto : uat._tk, deleted : false}, function(error, user) {
    if (error) return callback({"code" : 500}, null);

    if (!user) return callback({"code" : 402}, null);

    user.permissions.notifications = notif;

    user.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success"
      });
    });
  });
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function birthdateIsValid(birthdate, required) {
  const difference = moment().diff(birthdate, 'years');
  return difference >= required;
}

function generateRedeemToken() {
  var token = [];
  let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  let nums = Math.floor(Math.random() * (99999999 - 1000 + 1)) + 1000;
  if (nums.toString().length < 8) {
    token.push(nums.toString());
    let additions = "";
    for (var i = 0; i < 8 - nums.toString().length; i++) {
      additions += chars[Math.floor(Math.random() * chars.length)];
    }
    token.push(additions);
  } else {
    token.push(nums.toString().substr(0,4));
    token.push(nums.toString().substr(4,7));
  }

  return token.join("-");
}

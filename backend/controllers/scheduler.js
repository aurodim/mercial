const cron = require('node-schedule');
const moment = require('moment');
const mailer = require('./mailer');
const User = require('../models/user');
const Franchise = require('../models/franchise');
const Advertiser = require('../models/advertiser');
const stripe = require("stripe")(process.env.STRIPE_SECRET);

var exports = {};
exports.deleteAccount = DeleteAccount;
module.exports = exports;

function DeleteAccount(account, type) {
  cron.scheduleJob(moment().add(30, 'd').toDate(), () => {
    let Model;

    if (type === "client") {
      Model = User;
    } else if (type === "business") {
      Model = Franchise;
    } else {
      Model = Advertiser;
    }

    Model.findOne({_id : account._id}).exec((error, account) => {
      if (!account || !account.deleted) return;
      if (type === "business") {
        DeleteStripeAccount(account);
      } else if (type === "advertiser") {
        DeleteStripeCustomer(account);
      }
      account.remove((error) => {
        mailer.deletedAccount(account);
      });
    });
  });
}

function DeleteStripeCustomer(advertiser) {
  stripe.customers.deleteSource(
    advertiser.stripe.customer,
    advertiser.stripe.source,
    function(err, source) {}
  );
  stripe.customers.del(advertiser.stripe.customer, function(err, confirmation) {});
}

function DeleteStripeAccount(business) {
  stripe.accounts.del(business.stripe.customer);
}

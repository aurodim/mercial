const Mercial = require('../models/mercial.js');
const Advertiser = require('../models/advertiser.js');
const Admin = require('../models/admin.js');
const mongoose = require('mongoose');
const fs = require('fs'),
      path = require("path");
const formula = require("./formula.js");
const moment = require('moment');
const cloudinary = require('cloudinary');

var exports = {};
exports.all = All;
exports.of = Of;
exports.one = One;
exports.new = New;
exports.addView = AddView;
exports.update = Update;
exports.remove = Remove;
exports.addReport = AddReport;
module.exports = exports;

function All(query, service = 'f', limit = 100, skip = 0, sort = {created : -1}, raw = false, lang = "en") {
  return new Promise((resolve, reject) => {
    Mercial.find(query).sort(sort).limit(limit).skip(skip).populate('advertiser').exec(function(error, mercials) {
      if (error) return reject(error);

      if (raw) {
        return resolve(mercials);
      }

      FormulateJSON(mercials, service, false, lang).then(json => resolve(json));
    });
  });
}

function Of(query, service = 'f', lang = "en") {
  return new Promise((resolve, reject) => {
    Mercial.find(query).sort({created : -1}).populate('advertiser').exec(function(error, mercials) {
      if (error) return reject(error);

      FormulateJSON(mercials, service, false, lang).then(json => resolve(json)).catch((error) => reject(error));
    });
  });
}

function One(query, service = 'f', raw = false, lang = "en") {
  return new Promise((resolve, reject) => {
    Mercial.findOne(query).populate('advertiser').exec(function(error, mercial) {
      if (error) return reject(error);

      if (!mercial) return resolve(null);

      if (raw) {
        return resolve(mercial);
      }

      if (mercial.advertiser.credits == 0) {
        if (service != "adv") {
          if (!mercial.advertiser.stripe.cc || !mercial.advertiser.stripe.customer) return resolve(null);
        }

        if (!mercial.advertiser.stripe.cc || !mercial.advertiser.stripe.customer || !mercial.advertiser.stripe.source) return reject("_invalidpayment");
      }

      FormulateJSON([mercial], service, true, lang).then(json => resolve(json)).catch((error) => reject(error));
    });
  });
}

function AddView(viewer, query) {
  return new Promise((resolve, reject) => {
    One(query, 'f', true).then((mercial) => {

      if (viewer.views.length > 0) {
        let lastViewTime = moment(viewer.views[0].date);
        let startTime = moment().subtract(mercial.duration, 's');

        if (startTime.toDate() < lastViewTime.toDate() + 500) {
          return reject("_mta");
        }
      }

      mercial.views.push({
        viewer : mongoose.Types.ObjectId(viewer._id),
        date : moment().toDate()
      });
      mercial.viewsAmount += 1;
      mercial.save(function(error) {
        if (error) return reject(error);
        resolve(mercial);
      })
    });
  }).catch((error) => {
    reject(error);
  });
}

function New(advertiser, data, file) {
  const supportedLanguages = ["any", "en", "es"];
  return new Promise((resolve, reject) => {
    const id = mongoose.Types.ObjectId();
    let video = "";
    let attributes = [];
    const title = data.title.trim();
    const description = data.description.trim();
    let tags = data.tags.split(/\s*,\s*/).map((val) => val.trim().toUpperCase());
    const locationType = data.locationType.trim();
    let locations = data.locations.split(/\s*,\s*/).map((val) => val.trim().toUpperCase()).filter(val => val.trim() !== '');;
    let ethnicities = data.ethnicities.split(/\s*,\s*/).map((val) => val.trim()).filter(val => val.trim() !== '');
    let more = [];
    let languages = data.languages.split(/\s*,\s*/).filter((lang) => supportedLanguages.includes(lang));

    if (languages.length == 0) languages.push("any");
    if (ethnicities.length == 0) ethnicities.push("any");
    if (locations.length == 0) locations.push("any");

    FormulateVideo(advertiser, file, data.tags, id).then((res)=>{
      video = res.secure_url;

      let merchips = formula.getVideoMerchips(res.duration, data.size);

      let ages = data.age.split("-");
      let agesFormat = [];
      agesFormat  = agesFormat.map(age => parseInt(age.replace(/[^0-9.]/g, "")));
      if (agesFormat[0] > 99) {
        return reject("_invalidage");
      }
      if (ages.length > 1) {
        if (agesFormat[1] > 99) {
          return reject("_invalidage");
        }
        agesFormat.push(ages[0]);
        agesFormat.push(ages[1]);
      } else {
        agesFormat.push(ages[0]);
        agesFormat.push(99);
      }

      if (data.more.length > 0) {
        more.push(data.more);
      }

      if (data.moreSecond.length > 0 && (data.size == "medium" || data.size == "large")) {
        more.push(data.moreSecond);
      }

      if (data.moreThird.length > 0 && (data.size == "medium" || data.size == "large")) {
        more.push(data.moreThird);
      }

      if (data.moreFourth.length > 0 && data.size == "large") {
        more.push(data.moreFourth);
      }

      if (data.moreFifth.length > 0 && data.size == "large") {
        more.push(data.moreFifth);
      }

      if (data.moreSixth.length > 0 && data.size == "large") {
        more.push(data.moreSixth);
      }

      const random = Math.floor(Math.random() * 6);
      Admin.findOne().skip(random).exec(function (err, admin) {
        if (error) return reject(error);

        var mercial = new Mercial({
          _id : id,
          advertiser : advertiser._id,
          title : title,
          description : description.length > 0 ? description : null,
          tags : tags,
          metadata : {
            duration : res.duration,
            more : more.length > 0 ? more : null
          },
          target : {
            languages : languages,
            ethnicities : ethnicities,
            locationType : data.locationType === "" || data.locationType === "none" ? "none" : locationType,
            locations :  locations
          },
          moderation : {
            moderator: admin._id
          },
          forcedPause : advertiser.tab.paymentFailed,
          size : data.size,
          age : {
            lower : agesFormat[0],
            upper : agesFormat[1]
          },
          video : video,
          attributes : attributes,
          merchips : merchips,
          created : moment().toDate()
        });

        mercial.save(function(error, doc) {
          if (error) return reject(error);

          FormulateJSON([doc], 'f', true).then((json) => resolve(json)).catch(error => reject(error));
        });
      });
    }).catch((error)=>{
      reject(error);
    });
  });
}

function Update(data, advertiser = null) {
  return new Promise((resolve, reject) => {
    const title = data.title.trim();
    const description = data.description.trim();
    const more = data.more ? data.more.trim() : null;
    let tags = data.tags.split(/\s*,\s*/).map((val) => val.trim().toUpperCase());
    const size = data.size.split(" ")[0];

    let query = {};
    query._id = data.id;
    query.deleted = false;
    query.suspended = false;
    if (advertiser) {
      query.advertiser = advertiser;
    }
    One(query, 'f', true).then((mercial) => {

      mercial.title = title;
      mercial.description = description.length > 0 ? description : null;
      mercial.metadata.more = more ? more : null;
      mercial.tags = tags;
      mercial.size = size;
      mercial.merchips = formula.getVideoMerchips(mercial.metadata.duration, mercial.size);
      mercial.updated = moment().toDate();

      mercial.save(function(error, doc) {
        if (error) return reject(error);

        let cbd = {
          id : doc._id
        };

        resolve(cbd);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function Remove(data, advertiser = null) {
  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    if (advertiser) {
      query.advertiser = advertiser._id;
    }
    One(query, 'f', true).then((mercial) => {
      mercial.deleted = true;
      mercial.paused = true;
      mercial.save(function(error) {
        if (error) return reject(error);
      });

      cloudinary.v2.uploader.destroy(mercial.video,
      { resource_type: "video" },
      function(error, result) { });

      let cbd = {};

      resolve(cbd);
    });
  });
}

function AddReport(data, user) {
  const id = data.id;
  const reason = data.id;
  const expand = data.id;

  return new Promise((resolve, reject) => {
    let query = {};
    query._id = data.id;
    query.deleted = false;
    One(query, 'f', true).then((mercial) => {

      mercial.reports.push({
        reporter : user,
        reason : reason,
        expand : expand,
        date : moment().toDate()
      });

      if (mercial.reports > 8) {
        mailer.suspendedMercial(mercial.advertiser.email, mercial);
        mercial.suspended = true;
      }

      mercial.save((error) => {
        if (error) return reject(error);

        resolve(mercial);
      });
    }).catch((error) => {
      reject(error);
    });
  });
}

function FormulateVideo(advertiser, file, tags, id) {
  return new Promise((resolve, reject) => {
    const videopath = file.path;
    cloudinary.v2.uploader.upload_large(videopath,
     {
       resource_type: "video",
       folder : `${process.env.CLOUDINARY_FOLDER}/advertisers/${advertiser._id}/`,
       tags : tags,
       format: 'mp4',
       eager_async: true
      },
     function(error, result) {
       if (error) return reject(error);
       fs.unlink(videopath, (err) => {});
       resolve(result);
     });
  });
}

function FormulateJSON(arr, service, one = false, lang = "en") {
  let promises = [];

  for (let mercial of arr) {
    let element = {};
    if (mercial.video.indexOf(".") > 0) {
      element.thumbnail = mercial.video.substring(0, mercial.video.lastIndexOf(".")) + ".jpg";
    }
    element.id = mercial._id;
    element.publisher = mercial.advertiser ? mercial.advertiser.name : null;
    element.title = mercial.title;
    element.thumbnail = element.thumbnail;
    element.video = mercial.video;
    element.tags = mercial.tags;
    element.size = mercial.size;
    element.merchips = mercial.merchips;
    element.duration = parseFloat(mercial.metadata.duration.toFixed(2));
    element.description = mercial.description;
    element.more = mercial.metadata.more;
    element.attributes = mercial.attributes;
    if (service === 'adv') {
      element.paused = mercial.paused;
    }
    if (service === 'a') {
      element.reports = mercial.reports;
    }

    promises.push(element);
  }

  return Promise.all(promises).then((data) => {
    if (one) return data[0];

    return data;
  });
}

function translate(element, lang = "en", service = "_none_") {
  return new Promise((resolve, reject) => {
    // if (lang == "en") return resolve(element);
    //
    // googleTranslate.translate([element.title, element.description, ...element.tags], lang, (err, res) => {
    //   if (err || typeof res == "undefined") resolve(element);
    //   if (service != "adv") {
    //     element.title = res[0].translatedText;
    //     element.description = res[1].translatedText;
    //   }
    //   let arr = res.splice(2);
    //   element.tags = [];
    //   arr.forEach(tag => {
    //     element.tags.push(tag.translatedText);
    //   });
    //   resolve(element);
    // });
    resolve(element);
  });
}

const FAQ = require('../models/faq.js');

var exports = {};
exports.all = All;
module.exports = exports;

function All(service = 'f', type = ['client']) {
  if (typeof type == 'string') {
    type = [type];
  }
  type.push("all");
  return new Promise((resolve, reject) => {
    FAQ.find({type : {$in : type}}).sort({created : -1}).exec(function(error, faqs) {
      if (error) return reject(error);

      resolve(FormulateJSON(faqs, service));
    });
  });
}

function FormulateJSON(arr, service) {
  let json = [];

  if (service === 'f') {
    for (let faq of arr) {
      let element = {};
      element.type = faq.type;
      element.param = faq.param;
      element.title = faq.title;
      element.answer = faq.content;
      json.push(element);
    }
  } else {
    json = arr;
  }

  return json;
}

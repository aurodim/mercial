const Mercial = require("../models/mercial.js");
const Franchise = require("../models/franchise.js");
const User = require("../models/user.js");

var exports = {};
exports.advertiserDashboard = AdvertiserDashboard;
exports.franchiseDashboard = FranchiseDashboard;
exports.userDashboard = ClientDashboard;
module.exports = exports;

function AdvertiserDashboard(advertiser) {
  return new Promise((resolve, reject) => {
    let seconds = 0;
    let views = 0;

    let secv = new Array(12).fill(0);
    let adv = new Array(12).fill(0);
    let tagsObj = [];
    let tags = [];
    let tagsv = [];
    let topMercial = "";

    advertiser.mercials = advertiser.mercials.filter(mercial => !mercial.deleted).sort((a, b) => b.views.length - a.views.length);

    advertiser.mercials.forEach((mercial) => {

      mercial.views.forEach((view) => {
        seconds += mercial.metadata.duration;
        views += 1;
        adv[view.date.getMonth()] += 1;
        secv[view.date.getMonth()] += mercial.metadata.duration;
      });

      mercial.tags.forEach((tag) => {
        if (tags.indexOf(tag) == -1) {
          tagsObj.push({"name" : tag, "views" : mercial.views.length});
          tags.push(tag);
          tagsv.push(mercial.views.length);
        } else {
          tagsObj[tags.indexOf(tag)].views += mercial.views.length;
          tagsv[tags.indexOf(tag)] += mercial.views.length;
        }
      });
    });

    topMercial = advertiser.mercials.length > 0 ? advertiser.mercials[0].title : null;

    tags = [];
    tagsv = [];
    tagsObj.sort((a, b) => b.views - a.views);
    tagsObj.forEach(obj => {
      if (tags.length < 6) {
        tags.push(obj.name);
        tagsv.push(obj.views);
      }
    });

    let data = {
      _p : advertiser.mercials.length,
      _b : advertiser.tab.owe,
      _sv : seconds, //seconds viewed
      _v : views, // views
      _tm : topMercial, // Top Mercial Name
      _vs : {
        d : secv, // viewed seconds data
        c : fillColorsObject(1), // colors
      },
      _va : {
        d : adv, // viewed ads data
        c : fillColorsObject(1), // colors
      },
      _vt : {
        d : tagsv, // viewed tags data
        l : tags,
        colors : {
          fill : fillColors(tags.length),
          borders : fillWhite(tags.length)
        }
      }
    };

    resolve(data);
  });
}

function FranchiseDashboard(franchise) {
  return new Promise((resolve, reject) => {
    let locations = [];
    let orQuery = [];

    for (let location of franchise.address) {
      locations.push({
        formatted : location.formatted,
        lat : location.geo[1],
        lng : location.geo[0]
      });
      console.log(location.geo);
      orQuery.push({
        "target.zipcode.geo" : {
          $geoWithin : {$centerSphere : [ location.geo, 15 / 3963.2 ]}
        }
      });
    }

    let rank = 0;
    let revenue = 0;

    console.log(orQuery);

    Franchise.find({}).sort({redeems : 1}).exec(function(error, docs) {
      docs.forEach((franc, index) => {
        if (franc._id.toString() == franchise._id.toString()) {
          rank = index + 1;
          return;
        }
      });

      User.find({"target.zipcode.formatted" : {$ne : null}, $or : orQuery}).exec(function(error, users) {

        let cID = new Set();
        let ncd = new Array(12).fill(0);
        let rcd = new Array(12).fill(0);
        let und = new Array(12).fill(0);

        franchise.redeems.forEach((redeem) => {
          revenue+=redeem.revenue;
          if (cID.has(redeem.redeemer.toString())) {
            rcd[redeem.date.getMonth()] += 1;
          } else {
            cID.add(redeem.redeemer.toString());
            ncd[redeem.date.getMonth()] += 1;
          }
        });

        users.forEach((user) => {
          und[user.target.zipcode.updated.getMonth()] += 1;
        });

        let data = {
          _op : franchise.offers.length, // offers Posted
          _mm : revenue, // money made
          _ct : franchise.tab.amount,
          _or : franchise.redeems.length, // offers redeemed
          _r : rank, // rank
          _ra : franchise.rating, // rating franchise.rating
          _nm : users.length, // near me
          _l : locations, // locations
          _cd : {
            n : ncd, // new customers data
            r : rcd, // returning
            c : fillColorsObject(2), // colors
          },
          _nd : {
            d : und, // nearby data
            c : fillColorsObject(1), // colors
          }
        };

        resolve(data);
      });
      });
  });
}

function ClientDashboard(user) {
  return new Promise((resolve, reject) => {
    let merchips = 0;
    let saved = 0;
    let zipcodes = [];
    let zipApp = [];
    let spm = new Array(12).fill(0);
    let vpm = new Array(12).fill(0);
    let rpm = new Array(12).fill(0);
    let vrpm = new Array(12).fill(0);

    user.views.forEach((view)=> {
      merchips += view.merchips;
      spm[view.date.getMonth()] += view.mercial.metadata.duration;
      vpm[view.date.getMonth()] += 1;
      vrpm[view.date.getMonth()] += 1;
    });
    // user.revsAnswered.forEach((rev) => {
    //   if (typeof rev.merchips != "undefined") {
    //     merchips += rev.merchips;
    //   }
    // });
    merchips += user.awardedMerchips;
    user.redeemed.forEach((offer)=>{
      saved += offer.saved;
      vrpm[offer.date.getMonth()] += 1;
      rpm[offer.date.getMonth()] += 1;
      if (offer.zipcode && zipcodes.includes(offer.zipcode.split("-")[0])) {
        zipApp[zipcodes.indexOf(offer.zipcode.split("-")[0])] += 1;
      } else {
        if (offer.zipcode) {
          zipcodes.push(offer.zipcode.split("-")[0]);
          zipApp.push(1);
        }
      }
    });

    let data = {
      _me : merchips,
      _mm : user.merchips,
      _ms : saved,
      _or : user.redeemed.length,
      _mw : user.views.length,
      _ra : user.revsAnswered.length,
      _z : {
        labels : zipcodes,
        data : zipApp,
        colors : {
          fill : fillColors(zipcodes.length),
          borders : fillWhite(zipcodes.length)
        }
      },
      _md : {
        _or : rpm,
        _mo : vrpm,
        _mw : vpm,
        colors : fillColorsObject(3)
      }, vrpm,
      _smd : spm
    };

    resolve(data);
  });
}

function fillColorsObject(length) {
  return new Array(length).fill(null).map(
    val => {
      let colors = fillColors(1, true, true);
      return {
        backgroundColor: colors[1],
        borderColor: colors[0],
        pointBackgroundColor: colors[0],
        pointBorderColor: colors[0],
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: colors[0]
      };
    }
  );
}

function randomColor() {
  var letters = '0123456789ABCDEF';
  var color = '';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function fillColors(length, transparency = false, arraySep = false) {
  if (length == 1) {
    if (arraySep) {
      let color = randomColor();
      return transparency ? [`#${color}`,`${hexToRgbA(color)}`] : [`#${color}`];
    }
    return transparency ? `${hexToRgbA(randomColor())}` : `#${randomColor()}`;
  }

  return new Array(length).fill(null).map(val => {
    if (arraySep) {
      let color = randomColor();
      return transparency ? [`#${color}`,`${hexToRgbA(color)}`] : [`#${color}`];
    }
    return transparency ? `${hexToRgbA(randomColor())}` : `#${randomColor()}`;
  });
}

function hexToRgbA(hex, transparency = "0.2"){
    var c;
    if(/^([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+`,${transparency})`;
    }
    throw new Error('Bad Hex');
}

function fillWhite(length) {
  return new Array(length).fill("#fff");
}

Array.prototype.shuffle = function() {
  let newArr = [];

  while (this.length) {

   var randomIndex = Math.floor(Math.random() * this.length),
       element = this.splice(randomIndex, 1);

   newArr.push(element[0]);
  }

  return newArr;
};

function formatMercial(arr, service, one = false) {
  let json = [];

  for (let mercial of arr) {
    let element = {};
    if (mercial.video.indexOf(".") > 0) {
      element.thumbnail = mercial.video.substring(0, mercial.video.lastIndexOf(".")) + ".jpg";
    }
    element.id = mercial._id;
    element.publisher = mercial.advertiser.name || null;
    element.title = mercial.title;
    element.thumbnail = element.thumbnail;
    element.video = mercial.video;
    element.tags = mercial.tags;
    element.size = mercial.size;
    element.merchips = mercial.merchips;
    element.duration = parseFloat(mercial.metadata.duration.toFixed(2));
    element.description = mercial.description;
    element.more = mercial.metadata.more;
    element.attributes = mercial.attributes;
    if (service === 'adv') {
      element.paused = mercial.paused;
      element.views = mercial.viewsAmount;
    }
    json.push(element);
  }

  if (one) {
    return json[0];
  }

  return json;
}

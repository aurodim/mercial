const Advertiser = require('../models/advertiser.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const Analyzer = require("./analyzer.js");
const Mailer = require("./mailer.js");
const Mercials = require("./mercials.js");
const formula = require("./formula.js");
const FAQS = require("./faqs.js");
const stripe = require("stripe")(process.env.STRIPE_SECRET);
const moment = require('moment');
const scheduler = require("./scheduler");

var exports = {};
exports.all = All;
exports.one = One;
exports.createAdvertiser = CreateAdvertiser;
exports.loginAdvertiser = LoginAdvertiser;
exports.deleteAccount = DeleteAccount;
exports.analyticsData = AnalyticsData;
exports.helpData = HelpData;
exports.addToTab = AddToTab;
exports.mercialsData = MercialsData;
exports.uploadMercial = UploadMercial;
exports.pauseMercial = PauseMercial;
exports.updateMercial = UpdateMercial;
exports.deleteMercial = DeleteMercial;
exports.mercialAnalyticsData = MercialAnalyticsData;
exports.editorData = EditorData;
exports.settingsData = SettingsData;
exports.updateAccount = UpdateAccount;
exports.updateInfo = UpdateInfo;
exports.updatePayment = UpdatePayment;
module.exports = exports;

function All(query = {}, limit = 30, skip = 0) {
  return new Promise((resolve, reject) => {
    Advertiser.find(query).sort({created : -1}).limit(limit).skip(skip).populate('mercials').exec(function(error, users) {
      if (error) reject(error);

      resolve(users);
    });
  });
}

function One(query) {
  return new Promise((resolve, reject) => {
    Advertiser.findOne(query).populate("mercials").exec(function(error, user) {
      if (error) reject(error);

      resolve(user);
    });
  });
}

function CreateAdvertiser(body, callback) {
  const fullname = body.fullname;
  const email = body.email.toLowerCase().trim();

  Advertiser.findOne({email : email}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (advertiser) return callback(null, {_m : "_emailinuse", _tk : null});

    const key = crypto.randomBytes(5).toString("hex") + "." + crypto.randomBytes(7).toString("hex") + "." + crypto.randomBytes(4).toString("hex");
    const accesskey = keyGen(4, "num").toString() + keyGen(2, "alpha").toString() + keyGen(2, "alphanum").toString() + keyGen(4, "num").toString();
    var hashedKey = bcrypt.hashSync(accesskey, 13);

    var newAdvertiser = new Advertiser({
      crypto : key,
      fullname : fullname,
      email : email,
      accesskey : hashedKey,
      created : moment().toDate()
    });

    newAdvertiser.save(function(error, doc) {
      if (error) return callback({"code" : 500}, null);

      var token = jwt.sign({_tk : key}, process.env.JWT_A_SECRET, {expiresIn : 86400 * 365});

      Mailer.welcomeAdvertiser(doc, accesskey).then(() => callback(null, {
        _m : "Successfully created account",
        _tk : token
      })).catch((error) => {
        return callback({"code" : 500}, null);
      });
    });
  });
}

function LoginAdvertiser(body, callback) {
  const email = body.email.toLowerCase().trim();
  const accesskey = body.accesskey.replace(/-/g, "");

  Advertiser.findOne({email : email, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);
    if (!advertiser) return callback(null, {_m : "_emailinvalid", _tk : null});

    if (advertiser.locked) {
      return callback(null, {_m : "_accountlocked", _tk : null});
    }

    if (!bcrypt.compareSync(accesskey, advertiser.accesskey)) {
      advertiser.failed = advertiser.failed + 1;
      if (advertiser.failed >= 5) {
        advertiser.locked = true;
      }
      advertiser.save();
      return callback(null, {_m : "_passinvalid", _tk : null});
    }

    advertiser.deleted = false;
    advertiser.failed = 0;

    advertiser.save();

    var token = jwt.sign({_tk : advertiser.crypto}, process.env.JWT_A_SECRET, {expiresIn : 86400 * 365});

    callback(null, {
      _m : "Successfully logged in",
      _tk : token
    });
  });
}

function DeleteAccount(body, callback) {
  var aat = jwt.decode(body.auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}).exec(function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    if (advertiser.tab.owe != 0) {
      stripe.charges.create({
        amount: advertiser.tab.owe,
        currency: "usd",
        source: advertiser.stripe.source, // obtained with Stripe.js
        description: "Mercial for Advertisers charge for current tab on uploaded mercials"
      }, function(err, charge) {
        // asynchronously called
        if (err) {
          PaymentFailed(advertiser, error);
        } else {
          advertiser.tab.owe = 0;
          advertiser.tab.currentTabId = crypto.randomBytes(9).toString("hex");
          advertiser.tab.past.push(advertiser.tab.current);
          advertiser.tab.current = [];
        }
      });
    }
    advertiser.deleted = true;
    advertiser.updated = moment().toDate();
    advertiser.save(function(error) {
      if (error) return callback({"code" : 500}, null);

      scheduler.deleteAccount(advertiser, "advertiser");

      callback(null, {
        _m : "API deleted advertiser successfully",
        _tk : body.auth,
        _d : null
      });
    });
  });
}

function AnalyticsData(auth, callback) {
  var aat = jwt.decode(auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}).populate("mercials").exec(function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    Analyzer.advertiserDashboard(advertiser).then((data)=>{
      callback(null, {
        _m : "API returned analytics data successfully",
        _tk : auth,
        _d : data
      });
    }).catch((error) => {
      if (error) return callback({"code" : 500}, null);
    });
  });
}

function HelpData(auth, callback) {
  var aat = jwt.decode(auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    FAQS.all('f', ['advertiser']).then((faqs) => {
      callback(null, {
        _m : "API returned help data successfully",
        _tk : auth,
        _d : {
          _f : faqs
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function AddToTab(query, data) {
  return new Promise((resolve, reject) => {
    Advertiser.findOne(query, function(error, advertiser) {
      if (error) return reject(error);

      if (!advertiser) return reject();

      let price = formula.getVideoCost(data.seconds, data.size);

      if (advertiser.credits > 0) {
        advertiser.credits -= 1;
        if (advertiser.credits == 0) {
          PaymentFailed(advertiser, null, true);
        }
      } else {
        if (!advertiser.tab.currentTabId) {
          advertiser.tab.currentTabId = crypto.randomBytes(9).toString("hex");
        }
        advertiser.tab.owe += data.seconds * price;
        advertiser.tab.current.push({
          amount : price * data.seconds,
          mercial : data.mercial,
          date : moment().toDate(),
          tabId : advertiser.tab.currentTabId
        });

        if (advertiser.tab.owe > 10) {
          stripe.charges.create({
            amount: advertiser.tab.owe,
            currency: "usd",
            source: advertiser.stripe.source, // obtained with Stripe.js
            description: "Mercial for Advertisers charge for current tab on uploaded mercials"
          }, function(err, charge) {
            // asynchronously called
            if (err) {
              PaymentFailed(advertiser, err);
            } else {
              advertiser.tab.owe = 0;
              advertiser.tab.currentTabId = crypto.randomBytes(9).toString("hex");
              advertiser.tab.past.push(advertiser.tab.current);
              advertiser.tab.current = [];
            }
          });
        }
      }
      advertiser.save(function(error) {
        if (error) return reject(error);
        resolve();
      });
    });
  });
}

function MercialsData(data, callback) {
  var aat = jwt.decode(data.auth);
  let lang = "en";
  if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
    lang = data.lang;
  }
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    Mercials.of({advertiser : advertiser._id, deleted : false}, 'adv', lang).then((mercials) => {
      callback(null, {
        _m : "API returned help data successfully",
        _tk : data.auth,
        _d : {
          _c : mercials
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UploadMercial(body, file, callback) {
  const auth = body.auth;

  var aat = jwt.decode(auth);

  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    if (advertiser.credits == 0 && (!advertiser.stripe.customer || !advertiser.stripe.cc || !advertiser.stripe.source)) return callback(null, {
      _m : "_invalidpayment",
      _c : null
    });

    const size = body.size;

    if (size !== "small" && size !== "medium" && size !== "large") return callback(null, {
      _m : "_invalidsize",
      _c : null
    });

    Mercials.new(advertiser, body, file).then((mercial) => {
      advertiser.mercials.push(mercial.id);

      advertiser.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success",
          _c : mercial
        });
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function UpdateMercial(body, callback) {
  const auth = body.auth;

  var aat = jwt.decode(auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    const size = body.size;

    if (size !== "small" && size !== "medium" && size !== "large") return callback(null, {
      _m : "_invalidsize",
      _c : null
    });

    Mercials.update(body, advertiser._id).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function PauseMercial(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var aat = jwt.decode(auth);

  Mercials.one({_id : id, deleted : false}, 'f', true).then((mercial) => {
    if (mercial.advertiser.crypto !== aat._tk) return callback({"code" : 402}, null);

    if (!mercial) {
      return callback(null, {
        _m : "_notfound",
        _tk : auth,
        _d : {
          _p : null
        }
      });
    }

    mercial.paused = !mercial.paused;
    mercial.save(function(error, doc) {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "API returned mercial data successfully",
        _tk : auth,
        _d : {
          _p : mercial.paused
        }
      });
    });
  }).catch((error) => {
    return callback({"code" : 500}, null);
  });
}

function DeleteMercial(body, callback) {
  const auth = body.auth;
  const id = body.id;

  var aat = jwt.decode(auth);

  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    Mercials.remove(body, advertiser).then((result) => {
      callback(null, {
        _m : "_success"
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function MercialAnalyticsData(auth, id, callback) {
  var aat = jwt.decode(auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    Mercials.one({_id : id, advertiser : advertiser._id, deleted : false}, 'adv', true).then((mercial) => {

      if (!mercial) {
        return callback(null, {
          _m : "mercial_not_found",
          _tk : auth,
          _d : {
            _v : false
          }
        });
      }

      let audience = [];
      let views = mercial.viewsAmount;
      let externals = mercial.metadata.interest;

      let lifespan = Math.floor((moment().valueOf() - mercial.created.getTime()) / 1000 / 60 / 60 / 24);
      let chartData = [
        {data : [], labels : []}
      ];

      let a = {};
      for (let view of mercial.views) {
        if (a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`]) {
          a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`].views += 1;
          if (a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`][`${view.date.getMonth() + 1}/${view.date.getDate()}`]) {
            a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`][`${view.date.getMonth() + 1}/${view.date.getDate()}`] += 1;
          } else {
            a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`][`${view.date.getMonth() + 1}/${view.date.getDate()}`] = 1;
          }
        } else {
          a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`] = {
            views : 1
          };
          a[`${view.date.getMonth()+1}/${view.date.getFullYear()}`][`${view.date.getMonth() + 1}/${view.date.getDate()}`] = 1;
        }
        if (audience.indexOf(view.viewer.toString()) < 0) {
          audience.push(view.viewer.toString());
        }
      }

      for (let b in a) {
        chartData[0].data.push(a[b].views);
        chartData[0].labels.push(b);
        let d = [];
        let l = [];
        for (let c in a[b]) {
            if (c !== "views") {
              d.push(a[b][c]);
              l.push(c);
            }
        }
        chartData.push({
          data: d,
          labels: l
        });
      }

      callback(null, {
        _m : "API returned mercial data successfully",
        _tk : auth,
        _d : {
          _n : mercial.title,
          _a : audience.length,
          _vi : views,
          _e : externals,
          _l : lifespan,
          _c : chartData,
          _v : true
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function EditorData(data, id, callback) {
  var aat = jwt.decode(data.auth);
  let lang = "en";
  if (typeof data.lang !== "undefined" && data.lang && data.lang !== "undefined") {
    lang = data.lang;
  }
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    Mercials.one({_id : id, advertiser : advertiser._id, deleted : false}, 'adv', false, lang).then((mercial) => {

      if (!mercial) {
        return callback(null, {
          _m : "Mercial was not found",
          _tk : data.auth,
          _d : {
            _m : null
          }
        });
      }

      callback(null, {
        _m : "API returned mercial data successfully",
        _tk : data.auth,
        _d : {
          _m : mercial
        }
      });
    }).catch((error) => {
      return callback({"code" : 500}, null);
    });
  });
}

function SettingsData(auth, callback) {
  var aat = jwt.decode(auth);
  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 401}, null);

    let data = {
      "_a" : {
        "email" : advertiser.email
      },
      "_i" : {
        "name" : advertiser.name
      },
      "_p" : {
        "valid" : advertiser.stripe.cc != null,
        "end" : null,
        "failed" : advertiser.tab.paymentFailed
      }
    };

    if (advertiser.stripe.customer && advertiser.stripe.cc) {
      stripe.customers.retrieveCard(
        advertiser.stripe.customer,
        advertiser.stripe.cc,
        function(err, card) {

          data._p.end = card ? card.last4 : null;
          callback(null, {
            _m : "API returned settings data successfully",
            _tk : auth,
            _d : data
          });
        }
      );
    } else {
      callback(null, {
        _m : "API returned settings data successfully",
        _tk : auth,
        _d : data
      });
    }
  });
}

function UpdateAccount(body, callback) {
  const auth = body.auth;
  const email = body.email.toLowerCase().trim();
  var aat = jwt.decode(auth);

  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    Advertiser.findOne({crypto : {$ne : aat._tk}, email : email}, function(error, existingEmail) {
      if (error) return callback({"code" : 500}, null);

      if (existingEmail) return callback(null, {_m : "_emailinuse"});

      advertiser.email = email;

      advertiser.save((error) => {
        if (error) return callback({"code" : 500}, null);

        callback(null, {
          _m : "_success"
        });
      });
    });
  });
}

function UpdateInfo(body, callback) {
  const auth = body.auth;
  const name = body.name;
  var aat = jwt.decode(auth);

  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    advertiser.name = name;

    advertiser.save((error) => {
      if (error) return callback({"code" : 500}, null);

      callback(null, {
        _m : "_success"
      });
    });
  });
}

function UpdatePayment(body, callback) {
  const auth = body.auth;
  const token = body.token;
  var aat = jwt.decode(auth);

  Advertiser.findOne({crypto : aat._tk, deleted : false}, function(error, advertiser) {
    if (error) return callback({"code" : 500}, null);

    if (!advertiser) return callback({"code" : 402}, null);

    if (token === "_emptytkn") {
      removeCard();
    } else if (advertiser.stripe.customer == null) {
      stripe.customers.create({
        description: `Advertiser - ${advertiser.name}; Nickname - ${advertiser.nickname}`,
        source: token.id, // obtained with Stripe.js
        email: advertiser.email,
        name: advertiser.name
      }, function(err, customer) {
        if (err) return callback(null, {
          _m : "_stripeerror",
          _sm : err.message
        });

        advertiser.stripe.customer = customer.id;
        advertiser.stripe.cc = token.card.id;
        advertiser.stripe.source = token.id;

        advertiser.save((error) => {
          if (error) return callback({"code" : 500}, null);
          callback(null, {
            _m : "_success"
          });
        });
      });
    } else {
      stripe.customers.update(advertiser.stripe.customer, {
        source : token.id
      }, function(err, customer) {
        if (err) return callback(null, {
          _m : "_stripeerror",
          _sm : err.message
        });

        advertiser.stripe.cc = token.card.id;
        advertiser.stripe.source = token.id;

        if (advertiser.tab.paymentFailed) {
          stripe.charges.create({
            amount: advertiser.tab.owe,
            currency: "usd",
            source: advertiser.stripe.source, // obtained with Stripe.js
            description: "Mercial for Advertisers current tab charge for uploaded mercials"
          }, function(err, charge) {
            // asynchronously called
            if (err) {
              PaymentFailed(advertiser, error);
            } else {
              advertiser.tab.paymentFailed = false;
              advertiser.tab.owe = 0;
              advertiser.tab.currentTabId = crypto.randomBytes(9).toString("hex");
              advertiser.tab.past.push(advertiser.tab.current);
              advertiser.tab.current = [];
              advertiser.mercials.forEach(function (mercial) {
                mercial.forcedPause = false;
                mercial.save();
              });
            }
          });
        }

        advertiser.save((error) => {
          if (error) return callback({"code" : 500}, null);

          callback(null, {
            _m : "_success"
          });
        });
      });
    }

    function removeCard() {

      if (!advertiser.stripe.cc || !advertiser.stripe.source || advertiser.stripe.customer) return callback(null, {
        _m : "_success"
      });

      if (advertiser.tab.owe > 0) {
        stripe.charges.create({
          amount: advertiser.tab.owe,
          currency: "usd",
          source: advertiser.stripe.source, // obtained with Stripe.js
          description: "Mercial for Advertisers charge for current tab on uploaded mercials"
        }, function(err, charge) {
          // asynchronously called
          if (err) {
            PaymentFailed(advertiser, error);
          } else {
            advertiser.tab.owe = 0;
            advertiser.tab.currentTabId = crypto.randomBytes(9).toString("hex");
            advertiser.tab.past.push(advertiser.tab.current);
            advertiser.tab.current = [];
          }
        });
      }

      stripe.customers.deleteCard(
        advertiser.stripe.customer,
        advertiser.stripe.cc,
        function(err, confirmation) {
          if (err) return callback(null, {
            _m : "_stripeerror",
            _sm : err.message
          });

          stripe.customers.deleteSource(
            advertiser.stripe.customer,
            advertiser.stripe.source,
            function(err, source) {

              if (err) return callback(null, {
                _m : "_stripeerror",
                _sm : err.message
              });

              advertiser.stripe.cc = null;
              advertiser.stripe.source = null;
              advertiser.save((error) => {
                if (error) return callback({"code" : 500}, null);

                callback(null, {
                  _m : "_success"
                });
              });
            }
          );
        }
      );
    }
  });
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function keyGen(amount, type) {
  var nums = [0,1,2,3,4,5,6,7,8,9];
  var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
  var sortable = [];
  var result = [];

  if (type === "alpha") {
    sortable = [...alpha];
  } else if (type === "num") {
    sortable = [...nums];
  } else if (type === "alphanum") {
    sortable.push(...alpha);
    sortable.push(...nums);
  }

  for (var i = 0; i < amount; i++) {
    result.push(sortable[Math.floor(Math.random() * sortable.length)]);
  }

  return result.join("");
}

function PaymentFailed(advertiser, error, easyPause = false) {
  advertiser.tab.paymentFailed = !easyPause;
  advertiser.mercials.forEach((mercial) => {
    mercial.forcedPause = !easyPause;
    if (easyPause) {
      mercial.paused = true;
    }
    mercial.save();
  });

  advertiser.save();
  if (!easyPause) {
    Mailer.failedPayment(advertiser, advertiser);
  }
}

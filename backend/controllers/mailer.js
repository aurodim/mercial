const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const MAIL_FROM = 'noreply@mercial.io';
const moment = require('moment');

var exports = {};
exports.welcome = Welcome;
exports.welcomeAdvertiser = AdvertiserCreated;
exports.welcomeBusiness = BusinessCreated;
exports.resetCode = ResetCode;
exports.suspendedAccount = SuspendedAccount;
exports.suspendedOffer = SuspendedOffer;
exports.suspendedMercial = SuspendedMercial;
exports.failedPayment = FailedPayment;
exports.sendManagerCode = SendManagerCode;
exports.deletedAccount = DeletedAccount;
module.exports = exports;


function Welcome(recipient, type) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-ffabf1ac2930402e8e5aad6f407bf42a',
      dynamic_template_data: {
        fullname: recipient.fullname,
        email: recipient.email,
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function ResetCode(recipient, code, isFranchise = false) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-deb13eed0d3343e48420250131e041d3',
      dynamic_template_data: {
        fullname: isFranchise ? recipient.name : recipient.fullname,
        code: code
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function SuspendedAccount(recipient, cause) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-d56608c783b94b9b8f3d61eee3241863',
      dynamic_template_data: {
        fullname: recipient.fullname,
        cause_of_suspension: cause,
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function AdvertiserCreated(recipient, code) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-341b5bf194fb46598038fe15f5e0035d',
      dynamic_template_data: {
        fullname: recipient.fullname,
        email: recipient.email,
        code: code
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function FailedPayment(recipient) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-917a79eab5744e24938b1ee2a08432dd',
      dynamic_template_data: {
        fullname: recipient.fullname
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function SuspendedMercial(recipient, mercial, cause) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-88342a63038546c1afaf443ccfb3b8f5',
      dynamic_template_data: {
        fullname: recipient.fullname,
        mercial: mercial,
        cause_of_suspension: cause
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function BusinessCreated(recipient) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-299a6b8573844961be1f729ddfa67e1a',
      dynamic_template_data: {
        name: recipient.name,
        email: recipient.email
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function SuspendedOffer(recipient, offer, cause) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-26294239d08a40f7b873d276a20a5b0e',
      dynamic_template_data: {
        business_name: recipient.name,
        offer: offer,
        cause_of_suspension: cause
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function DeletedAccount(recipient) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-d2fa268cc3614c63b4a75a1403a5e8ae',
      dynamic_template_data: {
        fullname: recipient.fullname
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

function SendManagerCode(recipient, result) {
  return new Promise((resolve, reject) => {
    const msg = {
      to: recipient.email,
      from: MAIL_FROM,
      templateId: 'd-2ca3b2efb3474b59ba65b2fd537252e4',
      dynamic_template_data: {
        fullname: recipient.fullname,
        url: `https://res.cloudinary.com/aurodim/image/upload/w_600,h_600,e_colorize,co_rgb:663399/${result.public_id}.png`
      }
    };
    sgMail.send(msg, (error) => {
      if (error) return resolve(error);
      resolve();
    });
  });
}

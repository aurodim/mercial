exports = {};
module.exports = {

/*

  THESE ARE THE FORMULAS AND EQUATIONS FOR THE CALCULATIONS IN THE APP


  DO NO CHANGE UNLESS DIFFERENT PRICES ARE GIVEN OR THE EQUATIONS ARE OPTIMIZED

  MOST RECENT UPDATE: 08/02/2019

  ABOUT THE UPDATE:
    IMPLEMENTED THE REVIEWS SYSTEM
      THIS SYSTEM WILL GIVE BACK A SMALL AMOUNT OF POINTS (around 5 per question in the mercial)
      

  04/03/2019 - ABOUT THE UPDATE:
    IMPLEMENTED MOST RECENT SYSTEM OF PRICES
    GAVE BACK COMPANIES WANTED AMOUNT

*/

// m = amount of merchips per video
// s = length of video (seconds)
// r = size of the video [1-3]
// o = offer savings
// c = cost of video -> revenue in $USD
// o = offer profit in terms of USD
// v = # of views
// l = # of payout to franchise
// e = # of earnings from views
// g = Gross Margin in USD
// oM = offer price in terms of merchips
// oR = # of times offer will be redeemed
// vR =  # of views to redeem once;
// FRANCHISES GET profit -> 32.5%

/*

  VIDEO PRICES
    $0.002
    $0.003
    $0.004
*/

  // @DOCS
  // THIS FUNCTION WILL GIVE THE AMOUNT OF USD A VIDEO WILL COST
  getVideoCost : function(seconds, reach) {
    return parseFloat(seconds.toFixed(2)) * (0.001 + 0.001*SizeFormat(reach));
  },

  // @DOCS
  // THIS FUNCTION WILL GIVE THE AMOUNT OF MERCHIPS A VIDEO WILL GIVE
  getVideoMerchips : function(seconds, reach) {
    return Math.floor(parseFloat(seconds.toFixed(2)) * SizeFormat(reach));
  },

  // @DOCS
  // THIS FUNCTION RETURNS THE OFFER PRICE IN TERMS OF MERCHIPS
  getOfferPrice : function(refund) {
    return Math.ceil((refund/0.2) / 0.006);
  },

  // @DOCS
  // THIS FUNCTION RETURNS THE AMOUNT OF MERCHIPS A REVIEW WILL GIVE
  getReviewMerchips : function(questions) {
    let questionsArr = questions.split("*QU*");
    return 5 * questionsArr.length;
  }

};

function SizeFormat(r) {
  if (r === "small") {
    return 1;
  } else if (r === "medium") {
    return 2;
  } else if (r === "large") {
    return 3;
  }
}

const Code = require('../models/code.js');
const mongoose = require('mongoose');
const moment = require('moment');

var exports = {};
exports.new = New;
exports.verify = Verify;
exports.invalidate = Invalidate;
module.exports = exports;

function New({owner, type, code, days}) {
  return new Promise((resolve, reject) => {
    let newCode = new Code({
      code : code,
      type : type,
      for : owner,
      expires : moment().add(days, 'd').valueOf(),
      created : moment().toDate()
    });

    newCode.save(function(error) {
      if (error) return reject(error);

      resolve(null);
    });
  });
}

function Verify({owner, type, code}) {
  return new Promise((resolve, reject) => {
    Code.findOne({for : owner.toString(), code : code, type : type, valid : true, expires : {$gte : moment().valueOf()}}, function(error, code) {
      if (error) return reject(error);

      if (!code) resolve(null);

      resolve(code);
    });
  });
}

function Invalidate(code) {
  return new Promise((resolve, reject) => {
    code.valid = false;
    code.updated = moment().toDate();
    code.save(function(error) {
      resolve(null);
    });
  });
}

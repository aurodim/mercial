import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as ENV from '../../environments/environment';

@Component({
  selector: 'app-clover',
  templateUrl: './clover.component.html',
  styleUrls: ['./clover.component.scss']
})
export class CloverComponent {
  error = false;
  finished = false;
  success = false;
  enteredCode = false;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  onAccess($event: { target: { value: any; }; }) {
    const accessCode = $event.target.value;
    if (accessCode.length === 10) {
      this.enteredCode = true;
    } else {
      return;
    }

    this.route.queryParams.subscribe(params => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      this.http.post(ENV.environment.endpoint + '/api/franchise/clover/authenticate',
      {accessCode, merchantID: params.merchant_id, employeeID: params.employee_id,
         clientID: params.client_id, code: params.code},
       { headers }).subscribe(() => {
        this.finished = true;
        this.success = true;
      }, () => {
        this.finished = true;
        this.error = true;
      });
    });
  }

}

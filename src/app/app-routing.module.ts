import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { LegalComponent } from './landing/legal/legal.component';
import { StripeComponent } from './stripe/stripe.component';
import { FaqsComponent } from './faqs/faqs.component';
import { HelperComponent } from './helper/helper.component';
import { LocationsComponent } from './locations/locations.component';
import { BusinessesComponent } from './businesses/businesses.component';
import { AdvertisersComponent } from './advertisers/advertisers.component';
import { CloverComponent } from './clover/clover.component';
import { SquareComponent } from './square/square.component';

const routes: Routes = [
  { path : 'legal', component: LegalComponent },
  { path : 'helper', component: HelperComponent },
  { path : 'faqs', component: FaqsComponent },
  { path: 'locations', component: LocationsComponent },
  { path: 'businesses', component: BusinessesComponent },
  { path: 'advertisers', component: AdvertisersComponent },
  { path : 'stripe/verification', component: StripeComponent },
  { path : 'square/auth/success', component: SquareComponent },
  { path : 'clover/auth/success', component: CloverComponent },
  { path : '**', component: LandingComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

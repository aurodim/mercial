import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LegalComponent implements OnInit {
  active = 'privacy';
  guidelines = {
    advertiser: '',
    business: '',
    client: '',
    dmca: ''
  };
  loaded = false;

  constructor(private route: ActivatedRoute, private translateService: TranslateService, private http: HttpClient) {

  }

  ngOnInit() {
    this.route.fragment.subscribe((fragment: string) => {
        switch (fragment) {
          case 'privacy-policy':
            this.active = 'privacy';
            break;
          case 'terms-of-use':
            this.active = 'terms';
            break;
          case 'disclaimer':
            this.active = 'disclaimer';
            break;
          case 'cookies':
            this.active = 'cookies';
            break;
          case 'dmca':
            this.active = 'dmca';
            break;
          case 'advertiser-guidelines':
            this.active = 'advGuide';
            break;
          case 'business-guidelines':
            this.active = 'busiGuide';
            break;
          case 'client-guidelines':
            this.active = 'clientGuide';
            break;
        }
    });
    this.http.get(environment.endpoint + '/translations/' + this.translateService.getDefaultLang() + '.guidelines.json')
      .subscribe((data: any) => {
          this.guidelines.advertiser = data.advertiser;
          this.guidelines.business = data.business;
          this.guidelines.client = data.client;
          this.guidelines.dmca = data.dmca;
      });
  }

}

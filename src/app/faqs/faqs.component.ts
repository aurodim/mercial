import { Component, OnInit } from '@angular/core';
import { FAQ } from './faq.model';
import { APIService } from '../auth/api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  faqsBackup: FAQ[] = [];
  faqs: FAQ[] = [];
  types = ['client', 'business', 'advertiser'];
  fragment: any;
  keyword = '';

  constructor(private api: APIService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.api.helpData(this.types,
      (response: any) => {
        this.faqs = [];
        this.faqsBackup = [];
        const res = response._body._d;
        for (const faq of res._f) {
          this.faqsBackup.push(new FAQ(faq.type, faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.content)));
          this.faqs.push(new FAQ(faq.type, faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.content)));
        }
      }
    );
  }

  onDelete(type: string) {
    this.types = this.types.filter((arrType) => arrType !== type);
    if (this.types.length === 0) {
      this.faqs = [];
      this.faqsBackup = [];
      return;
    }
    this.api.helpData(this.types,
      (response: any) => {
        this.faqs = [];
        this.faqsBackup = [];
        const res = response._body._d;
        for (const faq of res._f) {
          this.faqsBackup.push(new FAQ(faq.type, faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.content)));
          this.faqs.push(new FAQ(faq.type, faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.content)));
        }
      }
    );
  }

  onClear() {
    this.keyword = '';
    this.faqs = this.faqsBackup;
  }

  onSearch(e: any) {
    this.faqs = this.faqsBackup.filter(faq => {
      return faq.title.includes(e.target.value) || faq.answer.toString().includes(e.target.value);
    });
  }
}

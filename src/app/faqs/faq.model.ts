export class FAQ {
  type: string[];
  param: string;
  title: string;
  answer: any;

  constructor(type: string[], param: string, title: string, answer) {
    this.type = type;
    this.param = param;
    this.title = title;
    this.answer = answer;
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { FAQ } from '../faqs/faq.model';

@Injectable()
export class APIService {
  public api: string;

  constructor(private http: HttpClient) {
    this.api = environment.endpoint;
  }

  // GET

  helpData(types: string[], onSuccess: (response: any) => void) {
    this.http.get(this.api +  '/api/faqs',
    { params : { types }, responseType : 'json' }).subscribe(
      (response: any) => onSuccess(response));
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { LandingComponent } from './landing/landing.component';
import { LegalComponent } from './landing/legal/legal.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StripeComponent } from './stripe/stripe.component';
import { APIService } from './auth/api.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { FaqsComponent } from './faqs/faqs.component';

import { MzChipModule } from 'ngx-materialize';
import { MzCardModule } from 'ngx-materialize';
import { MzInputModule } from 'ngx-materialize';
import { HelperComponent } from './helper/helper.component';
import { MzModalModule } from 'ngx-materialize';
import { MzButtonModule } from 'ngx-materialize';
import { UsMapModule } from 'ng4-us-map';
import { LocationsComponent } from './locations/locations.component';
import { MzCollapsibleModule } from 'ngx-materialize';
import { AdvertisersComponent } from './advertisers/advertisers.component';
import { BusinessesComponent } from './businesses/businesses.component';
import { CloverComponent } from './clover/clover.component';
import { SquareComponent } from './square/square.component';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, 'https://res.cloudinary.com/aurodim/raw/upload/v010/mercial/static/translations/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    LandingComponent,
    LegalComponent,
    StripeComponent,
    FaqsComponent,
    HelperComponent,
    LocationsComponent,
    AdvertisersComponent,
    BusinessesComponent,
    CloverComponent,
    SquareComponent
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InfiniteScrollModule,
    FormsModule,
    MzChipModule,
    MzCardModule,
    MzInputModule,
    MzModalModule,
    MzButtonModule,
    MzCollapsibleModule,
    UsMapModule,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
    HttpClientModule
  ],
  providers: [APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }

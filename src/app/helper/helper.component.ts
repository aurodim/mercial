import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-helper',
  templateUrl: './helper.component.html',
  styleUrls: ['./helper.component.scss', '../faqs/faqs.component.scss']
})
export class HelperComponent implements OnInit {
  tutorials = [
    {
      type: ['all'],
      title: 'An Introduction to Mercial',
      description: 'Find out what Mercial is all about!',
      param: 'mercial-summary',
      updated: '09/11/2019'
    },
    {
      type: ['client'],
      title: 'Mercial for Users',
      description: 'Find out how you can get exclusive discounts to your favorite shops by watching Mercials on your device',
      param: 'clients-beginner',
      updated: '09/11/2019'
    },
    {
      type: ['client'],
      title: 'App Rundown',
      description: 'Discover the different features within Mercial for Users',
      param: 'clients-explainer',
      updated: '09/11/2019'
    },
    {
      type: ['business'],
      title: 'Mercial for Businesses',
      description: 'Find out why Mercial is the app for your Business!',
      param: 'business-explainer',
      updated: '09/11/2019'
    },
    {
      type: ['advertiser'],
      title: 'Mercial for Advertisers',
      description: 'Find out why Mercial is the app you should be advertising on!',
      param: 'advertiser-explainer',
      updated: '09/11/2019'
    },
    {
      type: ['advertiser', 'business'],
      title: 'Advertiser\' and Business\' App Rundown',
      description: 'Discover the different features within Mercial for Advertisers and Mercial for Businesses',
      param: 'business-advertiser-beginner',
      updated: '09/11/2019'
    }
  ];
  selectedVideo: any = null;
  public modalOptions: Materialize.ModalOptions = {
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .8, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '8%', // Ending top style attribute
    ready: () => {},
    complete: () => {
      this.selectedVideo = null;
    } // Callback for Modal close
  };

  constructor() { }

  ngOnInit() {}

  onDelete(type: string) {
    this.tutorials = this.tutorials.filter(tutorial => {
      return !tutorial.type.includes(type);
    });
  }

  openVideo(i: number, modal) {
    this.selectedVideo = this.tutorials[i];
    modal.openModal();
  }
}

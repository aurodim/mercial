import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  supportedLanguages: string[] = ['en'];

  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('en');
    this.translate.use('en.client');
    (() => {
      const s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = 'https://app.termly.io/embed.min.js';
      s.id = '1a743334-1dbb-4853-a4b3-35e6fbc69847';
      s.setAttribute('data-name', 'termly-embed-banner');
      const x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    })();
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as ENV from '../../environments/environment';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit {
  error = false;
  finished = false;
  success = false;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      this.http.post(ENV.environment.endpoint + '/api/franchise/stripe/verify',
      {state: params.state, code: params.code}, { headers }).subscribe(() => {
        this.finished = true;
        this.success = true;
        this.afterFinish();
      }, () => {
        this.finished = true;
        this.error = true;
        this.afterFinish();
      });
    });
  }

  afterFinish() {
    setTimeout(() => {
      window.open('about:blank', '_self');
    }, 800);
  }

}

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as ENV from '../../environments/environment';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent {
  error = false;
  finished = false;
  success = false;
  enteredCode = false;

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  onAccess($event: { target: { value: any; }; }) {
    const accessCode = $event.target.value;
    if (accessCode.length === 10) {
      this.enteredCode = true;
    } else {
      return;
    }

    this.route.queryParams.subscribe(params => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      this.http.post(ENV.environment.endpoint + '/api/franchise/square/authenticate',
      {accessCode, code: params.code},
       { headers }).subscribe(() => {
        this.finished = true;
        this.success = true;
      }, () => {
        this.finished = true;
        this.error = true;
      });
    });
  }

}

import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements AfterViewInit {
  acceptedStates = ['IL'];

  constructor() { }

  ngAfterViewInit() {
    const states = document.getElementsByTagName('tspan') as unknown as any[];
    let state: HTMLElement;
    for (state of states) {
      if (this.acceptedStates.includes(state.innerHTML)) {
        state.style.fill = '#fff';
      }
    }
  }

}

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

const express = require('express');
const app = express();

const cors = require('cors');
const createError = require('http-errors');
const path = require('path');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const initRoutes = require('./backend/routes/init');
const compression = require('compression');

const options = {
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  useNewUrlParser: true
};
mongoose.connect(process.env.MONGODBURI, options).then(
  () => {
    console.log("CONNECTED TO MONGODB");
  },
  err => {
      console.log(err);
  }
);

function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if (!req.secure && req.get('x-forwarded-proto') !== 'https' && process.env.NODE_ENV !== "development") {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}

const allowedOrigins = [
  'https://app.mercial.io',
  'https://admin.mercial.io',
  'file://',
  'capacitor://localhost',
  'ionic://localhost',
  'http://localhost',
  'http://localhost:8080',
  'http://localhost:8100',
  'http://localhost:4200',
  '*.forestadmin.com',
  '*.clover.com',
  'http://app.forestadmin.com',
  'https://app.forestadmin.com',
  'https://www.mercial.io'
];

const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.includes(origin) || !origin || process.env.NODE_ENV !== 'production') {
      callback(null, true);
    } else {
      callback(new Error('Origin not allowed by CORS'));
    }
  },
  allowedHeaders: ['Authorization', 'X-Requested-With', 'Content-Type'],
  credentials: true // This is important.
}

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.set('view cache', true);

app.use(requireHTTPS);
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb'
}));
app.options("*", cors(corsOptions));
app.use(cors(corsOptions));
app.use(helmet({
  frameguard: {
    action: 'deny'
  }
}));
app.use(require('forest-express-mongoose').init({
  modelsDir: __dirname + '/backend/models',
  envSecret: process.env.FOREST_ENV_SECRET,
  authSecret: process.env.FOREST_AUTH_SECRET,
  mongoose: mongoose,
  integrations: {
    stripe: {
      apiKey: process.env.STRIPE_SECRET,
      mapping: 'businesses.stripe.customer',
      stripe: require('stripe')
    },
    stripe: {
      apiKey: process.env.STRIPE_SECRET,
      mapping: 'advertisers.stripe.customer',
      stripe: require('stripe')
    }
  }
}));
app.use(compression());
app.set('etag', false);
app.use('/translations', express.static(path.join(__dirname, 'backend/public/translations')));
app.use(express.static('dist/mercial'));
app.use(logger('dev'));
app.use('/', initRoutes);

module.exports = app;

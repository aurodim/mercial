# Mercial

Mercial is a project made with the intention of providing a three-piece platform which everyone-advertisers, businesses, and the everyman-can use and benefit from. Mercial’s system is divided into three different user interfaces and services: regular users, businesses, and advertisers. The name Mercial derives from the shortening of the words commercial and infomercial. How does it work? Watch short videos, Mercials, and receive points which can be redeemed at local businesses for discounts and coupons

## Need More Info?

For more information visit [Mercial's site](https://www.mercial.io)

## Contributing

This project can only be cloned and updated by authorized members.

## Author

This project was made possible by the founders of [Aurodim LLC](https://www.aurodim.com/)
